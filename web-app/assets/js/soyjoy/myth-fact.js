$(document).ready(function() {
  $('input[name="uploads"]').change(function(){
      var fileName = $(this).val();
      $('.image_name').text(fileName);
  });

  $('.input-text').jqte({
    color: false,
    fsize: false,
    format: false
  });

  $('.jqte .jqte_toolbar').prepend('<div class="title" style="padding-left: 5px;">Result</div>');

  $('.dates').datepicker({
    dateFormat: "yy-mm-dd"
  });
})