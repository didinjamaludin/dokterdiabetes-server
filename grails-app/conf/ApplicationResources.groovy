modules = {
    application {
        resource url:'js/application.js'
    }
	
	template {
		resource url:'assets/js/template.js'
	}
	
	article {
		resource url:'assets/js/soyjoy/article.js'
	}
	
	mythfact {
		resource url:'assets/js/soyjoy/myth-fact.js'
	}
}