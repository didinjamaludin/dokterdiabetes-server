class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: 'doctor')
        "500"(view:'/error')
		"/admin"(controller: 'admin')
	}
}
