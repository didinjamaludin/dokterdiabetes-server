import com.eximiomedia.auth.*
import cr.co.arquetipos.password.PasswordTools
import com.eximiomedia.AppUser

import javax.servlet.http.HttpServletRequest

class BootStrap {

	def init = { servletContext ->
		def roleAdmin = Authority.findByAuthority('ROLE_ADMIN')?:new Authority(authority:'ROLE_ADMIN').save(flush:true)
		def roleDoc = Authority.findByAuthority('ROLE_DOCTOR')?:new Authority(authority:'ROLE_DOCTOR').save(flush:true)

		def userAdmin = User.findByUsername('admin@dokterdash.com')?:new User(username:'admin@dokterdash.com',password:'123456',fullname:'Doctor Dashboard Admin',specialist:'-',hospital:'-').save(flush:true)

		if (!userAdmin.authorities.contains(roleAdmin)) {
			UserAuthority.create userAdmin, roleAdmin
		}

		def userDoc3 = User.findByUsername('endang@meetdoctor.com')?:new User(username:'endang@meetdoctor.com',password:'q1w2e3r4',fullname:'Endang Mintyaningsih',specialist:'Ahli Gizi',photo:'endang.jpg',hospital:'Antam Medika Hospital').save(flush:true)

		if (!userDoc3.authorities.contains(roleDoc)) {
			UserAuthority.create userDoc3, roleDoc
		}

		def userDoc = User.findByUsername('dipdo@meetdoctor.com')?:new User(username:'dipdo@meetdoctor.com',password:'q1w2e3r4',fullname:'dr. Dipdo Petrus Widjaya, SpPD',specialist:'Spesialis Penyakit Dalam',photo:'dipdo.jpg',hospital:'RSUD Tangerang dan GETHealty Clinic').save(flush:true)

		if (!userDoc.authorities.contains(roleDoc)) {
			UserAuthority.create userDoc, roleDoc
		}

		def userDoc2 = User.findByUsername('friens@meetdoctor.com')?:new User(username:'friens@meetdoctor.com',password:'q1w2e3r4',fullname:'dr. Friens Sinaga, Sp.JP.FIHA',specialist:'Spesialis Jantung dan Pembuluh Darah',photo:'friens.jpg',hospital:'RS Siloam Kebon Jeruk').save(flush:true)

		if (!userDoc2.authorities.contains(roleDoc)) {
			UserAuthority.create userDoc2, roleDoc
		}
	}
	def destroy = {
	}
}
