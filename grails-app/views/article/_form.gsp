<%@ page import="com.eximiomedia.Article"%>

<div
	class="fieldcontain ${hasErrors(bean: articleInstance, field: 'articleTitle', 'error')} form-title">
	<div class="title nav">
		<span class="left">Title</span>
	</div>
	<div class="input">
		<g:textField name="articleTitle"
			value="${articleInstance?.articleTitle}"
			placeholder="Tulis Judul Artikel" />
	</div>

</div>
<div
	class="fieldcontain ${hasErrors(bean: articleInstance, field: 'articleImage', 'error')} form-image">
	<div class="title nav">
		<span class="left">Image</span>
	</div>
	<div class="input">
		<div class="input-container">
			<div class="browse-image btn-blue">
				<span class="icon"> <input type="file"
					name="articleImageFile"> <g:if
						test="${params.action.equals('edit')}">
						<g:hiddenField name="articleImage"
							value="${articleInstance?.articleImage}" />
					</g:if>
				</span> <span class="text">Browse Image</span>
			</div>
			<span class="image_name"></span>
		</div>
	</div>
</div>

<div
	class="fieldcontain ${hasErrors(bean: articleInstance, field: 'articleContent', 'error')} form-textarea">
	<ckeditor:editor name="articleContent" height="100px" width="100%">
		${articleInstance?.articleContent}
	</ckeditor:editor>

</div>

