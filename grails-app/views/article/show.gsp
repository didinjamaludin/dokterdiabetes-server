
<%@ page import="com.eximiomedia.Article"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'article.label', default: 'Article')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div id="show-article" class="content scaffold-show" role="main">
		<h1>
			<g:message code="default.show.label" args="[entityName]" />
		</h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<table style="width:80%">
			<tbody>
				<tr>
					<td width="20%"><g:if test="${articleInstance?.articleImage}">
							<img
								src="http://articles.appdokter.com/${articleInstance?.articleImage}"
								height="200px" />
						</g:if></td>
					<td width="80%"><g:if test="${articleInstance?.articleTitle}">
							<g:fieldValue bean="${articleInstance}" field="articleTitle" />
						</g:if><br> <g:if test="${articleInstance?.createDate}">
							<g:formatDate date="${articleInstance?.createDate}"
								format="dd MMM yyyy" />
						</g:if><br> <g:if test="${articleInstance?.articleAuthor}">
				Create By:<g:fieldValue bean="${articleInstance}"
								field="articleAuthor" />
						</g:if></td>
				</tr>
				<tr>
					<td colspan="2"><g:if
							test="${articleInstance?.articleContent}">
							${raw(articleInstance?.articleContent)}
						</g:if>
						<hr> <g:if test="${articleInstance?.articleRefence}">
							<g:fieldValue bean="${articleInstance}" field="articleRefence" />
						</g:if></td>
				</tr>
				<tr>
					<td colspan="2"><b>Comments:</b></td>
				</tr>
				<g:if test="${articleInstance?.comments}">
					<g:each in="${articleInstance?.comments}" var="c" status="s">
					<tr>
						<td style="border-bottom:1px solid #ccc;padding-right:10px;">${c?.comment }
							<g:if test="${c.replies}">
								<g:each in="${c.replies}" var="r" status="st">
									<br><b>Reply:</b> ${r.replyComment}
								</g:each>
							</g:if>
						</td>
						<td style="border-bottom:1px solid #ccc;"><g:form action="replyComment" style="float:left;"><g:textField name="comment" placeholder="write reply" style="padding:3px;width:300px;" required="" /><g:hiddenField name="cid" value="${c.id}" /><g:hiddenField name="artid" value="${articleInstance?.id}" />&nbsp;<a href="#" onclick="$(this).parent().submit()"><i class="fa fa-reply"></i>Reply</a></g:form>&nbsp;|&nbsp;<g:link action="deleteComment" id="${c.id}" params="[artid:articleInstance.id]" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"><i class="fa fa-trash"></i>Delete</g:link></td>
					</tr>
					</g:each>
				</g:if>
			</tbody>
		</table>
		<g:form url="[resource:articleInstance, action:'delete']"
			method="DELETE">
			<fieldset class="buttons">
				<g:link class="edit" action="edit" resource="${articleInstance}">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:link>
				<g:actionSubmit class="delete" action="delete"
					value="${message(code: 'default.button.delete.label', default: 'Delete')}"
					onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
			</fieldset>
		</g:form>
	</div>
</body>
</html>
