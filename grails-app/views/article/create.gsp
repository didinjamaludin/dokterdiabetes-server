<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'article.label', default: 'Article')}" />
<title><g:message code="default.create.label"
		args="[entityName]" /></title>
<g:javascript library="article" />
</head>
<body>
	<div class="article-add">
		<div class="nav nav-article">
			<div class="left">
				<span class="icon"></span> <span class="text"
					onclick="window.history.back()">List Article</span>
			</div>
			<div class="right">
				<div class="btn-blue cursor">
					<span class="icon"></span>
					<g:link action="create">
						<span class="text">add new article</span>
					</g:link>
				</div>
			</div>
		</div>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<g:hasErrors bean="${articleInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${articleInstance}" var="error">
					<li
						<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
							error="${error}" /></li>
				</g:eachError>
			</ul>
		</g:hasErrors>
		<div class="container">
			<g:form url="[resource:articleInstance, action:'save']"
				enctype="multipart/form-data" id="art-form">
				<g:render template="form" />
				<div class="form-button right">
					<div class="calendar">
						<g:datePicker name="publishSchedule" precision="day"
							value="${articleInstance?.publishSchedule}" />
					</div>
					<div class="schedule">
						<span>Schedule</span>
						<g:checkBox name="isSchedule"
							value="${articleInstance?.isSchedule}" />
					</div>
					<div class="btn-submit">
						<div class="btn-blue publish">
							<span class="icon"></span> <span class="text"
								onclick="$('#art-form').submit()">Publish This Post</span>
						</div>
					</div>
				</div>
			</g:form>
		</div>
	</div>
</body>
</html>
