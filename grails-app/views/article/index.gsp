
<%@ page import="com.eximiomedia.Article"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'article.label', default: 'Article')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="article-list">
		<div class="nav nav-article">
			<div class="left">
				<span class="text">List Article</span>
			</div>
			<div class="right">
				<div class="btn-blue cursor">
					<span class="icon"></span>
					<g:link action="create">
						<span class="text">add new article</span>
					</g:link>
				</div>
			</div>
		</div>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<div class="lists">
			<div class="list-item">
				<table>
					<g:each in="${articleInstanceList}" status="i"
						var="articleInstance">
						<tr>
							<td class="number">
								${i+1}
							</td>
							<td class="review"><g:link action="show"
									id="${articleInstance.id}">
									<span class="title"> ${fieldValue(bean: articleInstance, field: "articleTitle")}
									</span>
								</g:link> <span class="description"> <g:if test="${articleInstance?.articleContent?.size()>300}">${raw(articleInstance?.articleContent[0..300])}..</g:if>
								<g:else>${raw(articleInstance?.articleContent)}</g:else>
							</span></td>
							<td class="launch"><span class="publish">Publish </span> <span
								class="text">: <g:formatDate
										date="${articleInstance?.publishSchedule}" format="dd.MM.yy" /></span></td>
							<td class="action"><g:form
									url="[resource:articleInstance, action:'delete']"
									method="DELETE">
									<div class="list">
										<g:link class="edit" action="edit"
											resource="${articleInstance}">
											<div class="btn-blue edit">
												<span class="icon"></span> <span class="text">Edit</span>
											</div>
										</g:link>
										<div class="btn-blue delete">
											<span class="icon"></span> <span class="text"><g:actionSubmit
													class="delete" action="delete"
													value="${message(code: 'default.button.delete.label', default: 'Delete')}"
													onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
										</div>
									</div>
								</g:form></td>
						</tr>
					</g:each>
				</table>
			</div>
		</div>
		<div class="pagination">
			<g:paginate total="${articleInstanceCount ?: 0}" />
		</div>
	</div>
</body>
</html>
