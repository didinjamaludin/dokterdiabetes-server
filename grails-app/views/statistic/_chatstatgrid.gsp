<g:if test="${chatInstanceList}">
<div class="lists">
			<div class="list-item">
			<h2>Total User: ${chatCount}</h2>
			<div class='export'>
  <span class='menuButton'>
    <a class='csv' href='${request.contextPath}/statistic/chatstat?formatfile=csv&amp;extension=csv'>CSV</a>
  </span>
  <span class='menuButton'>
    <a class='excel' href='${request.contextPath}/statistic/chatstat?formatfile=excel&amp;extension=xls'>EXCEL</a>
  </span>
  <span class='menuButton'>
    <a class='ods' href='${request.contextPath}/statistic/chatstat?formatfile=ods&amp;extension=ods'>ODS</a>
  </span>
  <span class='menuButton'>
    <a class='pdf' href='${request.contextPath}/statistic/chatstat?formatfile=pdf&amp;extension=pdf'>PDF</a>
  </span>
  <span class='menuButton'>
    <a class='rtf' href='${request.contextPath}/statistic/chatstat?formatfile=rtf&amp;extension=rtf'>RTF</a>
  </span>
  <span class='menuButton'>
    <a class='xml' href='${request.contextPath}/statistic/chatstat?formatfile=xml&amp;extension=xml'>XML</a>
  </span>
</div>
    <table>
        <thead>
            <tr>
                <g:sortableColumn property="sendDate"
                title="Date" />
                <g:sortableColumn property="doctor"
                title="Doctor" />
                <g:sortableColumn property="user"
                title="User" />
                <g:sortableColumn property="sender"
                title="Sender" />
                <g:sortableColumn property="message"
                title="Message" />
                <g:sortableColumn property="status"
                title="Status" />
            </tr>
        </thead>
        <tbody>
            <g:each in="${chatInstanceList}" status="i" var="chatInstance">
                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                    <td style="text-align: left"><g:formatDate date="${chatInstance?.sendDate}" format="dd.MM.yyyy"/></td>
                    <td style="text-align: left">${chatInstance?.doctor?.fullname}</td>
                    <td style="text-align: left">${chatInstance?.user?.nama}</td>
                    <td style="text-align: left">${chatInstance?.sender}</td>
                    <td style="text-align: left">${chatInstance?.message}</td>
                    <td style="text-align: left">${chatInstance?.status}</td>
                </tr>
            </g:each>
        </tbody>
    </table>
    </div></div>
    <div class="pagination">
        <g:paginate total="${chatInstanceTotal}" params="${filters}" />
    </div>
</g:if>
<g:else>
    <p>No Chats Found</p>
</g:else>