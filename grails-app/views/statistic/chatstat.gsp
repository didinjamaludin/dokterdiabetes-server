<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="layout" content="main" />
<title>Dokter Diabetes - Statistics</title>
</head>
<body>
	<div style="padding: 0 10px">
		<h1>Chat Statistics</h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<div id="userstat">
			<div id="filters">
				<select name="bydoctor" id="bydoctor" onchange="filterlist()">
					<option value="">Find By Doctor</option>
					<option value="2">Endang Mintyaningsih</option>
					<option value="3">dr. Dipdo Petrus Widjaya, SpPD</option>
					<option value="4">dr. Friens Sinaga, Sp.JP.FIHA</option>
				</select> Date From
				<g:textField name="datefrom" id="datefrom" value="${datefrom}" onchange="filterlist()" />
				Date To
				<g:textField name="dateto" id="dateto" value="${datefrom}" onchange="filterlist()" />
				<select name="bysender" id="bysender" onchange="filterlist()">
					<option value="">Find By Sender</option>
					<option value="doctor">Doctor</option>
					<option value="user">User</option>
				</select>
				<g:textField name="byuser" id="byuser" onkeyup="filterlist()" placeholder="Find By User Fullname" />
			</div>
			<div id="chatstatgrid">
				<g:render template="chatstatgrid" />
			</div>
		</div>
	</div>

	<script>
        function filterlist() {
                $.ajax({
                    url: "${request.contextPath}/statistic/chatstat",
					data : 'bydoctor=' + $("#bydoctor").val() + '&datefrom=' + $("#datefrom").val() + '&dateto=' + $("#dateto").val() + '&bysender=' + $("#bysender").val() + '&byuser=' + $("#byuser").val(),
					success : function(msg) {
						document.getElementById('chatstatgrid').innerHTML = msg;
					}
				});
		}

		$(document).ready(function() {
			$("#datefrom,#dateto").datepicker({ dateFormat: 'dd/mm/yy' });
		});
	</script>
</body>
</html>