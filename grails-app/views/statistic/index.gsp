<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="layout" content="main" />
<title>Dokter Diabetes - Statistics</title>
</head>
<body>
	<div style="padding: 0 10px">
		<h1>Statistics</h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<div id="userstat">
			<table>
				<tbody>
					<tr>
						<td>Total Users :</td>
						<td>${totalUser}</td>
					</tr>
					<tr>
						<td>Total Profiles :</td>
						<td><a class='excel' href='${request.contextPath}/statistic/profilelist?formatfile=excel&amp;extension=xls'>${totalProfile}</a></td>
					</tr>
					<tr>
						<td>Total Questions :</td>
						<td><g:link action="queststat">${totalQuest}</g:link></td>
					</tr>
					<tr>
						<td>Total Chats :</td>
						<td><g:link action="chatstat">${totalChat}</g:link></td>
					</tr>
					<tr>
						<td>Total Push Registration :</td>
						<td><g:link action="pushreg">${totalpush}</g:link>&nbsp;&nbsp;|&nbsp;&nbsp;<g:link action="refreshpush">Refresh</g:link></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>