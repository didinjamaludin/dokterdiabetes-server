<g:if test="${questInstanceList}">
<div class="lists">
			<div class="list-item">
			<h2>Total User: ${questCount}</h2>
			<div class='export'>
  <span class='menuButton'>
    <a class='csv' href='${request.contextPath}/statistic/queststat?formatfile=csv&amp;extension=csv'>CSV</a>
  </span>
  <span class='menuButton'>
    <a class='excel' href='${request.contextPath}/statistic/queststat?formatfile=excel&amp;extension=xls'>EXCEL</a>
  </span>
  <span class='menuButton'>
    <a class='ods' href='${request.contextPath}/statistic/queststat?formatfile=ods&amp;extension=ods'>ODS</a>
  </span>
  <span class='menuButton'>
    <a class='pdf' href='${request.contextPath}/statistic/queststat?formatfile=pdf&amp;extension=pdf'>PDF</a>
  </span>
  <span class='menuButton'>
    <a class='rtf' href='${request.contextPath}/statistic/queststat?formatfile=rtf&amp;extension=rtf'>RTF</a>
  </span>
  <span class='menuButton'>
    <a class='xml' href='${request.contextPath}/statistic/queststat?formatfile=xml&amp;extension=xml'>XML</a>
  </span>
</div>
    <table>
        <thead>
            <tr>
                <g:sortableColumn property="questDate"
                title="Question Date" />
                <g:sortableColumn property="doctor"
                title="Doctor" />
                <g:sortableColumn property="user"
                title="User" />
                <g:sortableColumn property="question"
                title="Question" />
                <g:sortableColumn property="answer"
                title="Answer" />
                <g:sortableColumn property="ansDate"
                title="Answer Date" />
            </tr>
        </thead>
        <tbody>
            <g:each in="${questInstanceList}" status="i" var="questInstance">
                <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
                    <td width="5%" style="text-align: left"><g:formatDate date="${questInstance?.questDate}" format="dd.MM.yyyy"/></td>
                    <td width="18%" style="text-align: left">${questInstance?.doctor?.fullname}</td>
                    <td width="15%" style="text-align: left">${questInstance?.user?.nama}</td>
                    <td width="27%" style="text-align: left">${questInstance?.question}</td>
                    <td width="30%" style="text-align: left">${questInstance?.answer}</td>
                    <td width="5%" style="text-align: left"><g:formatDate date="${questInstance?.ansDate}" format="dd.MM.yyyy"/></td>
                </tr>
            </g:each>
        </tbody>
    </table>
    </div></div>
    <div class="pagination">
        <g:paginate total="${questInstanceTotal}" params="${filters}" />
    </div>
</g:if>
<g:else>
    <p>No Chats Found</p>
</g:else>