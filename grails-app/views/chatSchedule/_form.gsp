<%@ page import="com.eximiomedia.ChatSchedule" %>

<div class="fieldcontain ${hasErrors(bean: chatScheduleInstance, field: 'dokter', 'error')} ">
	<label for="dokter">
		<g:message code="chatSchedule.dokter.label" default="Dokter" />
		
	</label>
	<g:select id="dokter" name="dokter.id" from="${com.eximiomedia.auth.UserAuthority.findAllByAuthority(com.eximiomedia.auth.Authority.findByAuthority("ROLE_DOCTOR")).user}" optionKey="id" required="" value="${chatScheduleInstance?.dokter?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: chatScheduleInstance, field: 'day', 'error')} ">
	<label for="day">
		<g:message code="chatSchedule.day.label" default="Day" />
		
	</label>
	<g:select name="day" from="${['Minggu','Senin','Selasa','Rabu','Kamis','Jum\'at','Sabtu']}" value="${chatScheduleInstance?.day}" />

</div>


<div class="fieldcontain ${hasErrors(bean: chatScheduleInstance, field: 'startTime', 'error')} ">
	<label for="startTime">
		<g:message code="chatSchedule.startTime.label" default="Start Time" />
		
	</label>
	<g:textField name="startTime" value="${chatScheduleInstance?.startTime}" />

</div>

<div class="fieldcontain ${hasErrors(bean: chatScheduleInstance, field: 'endTime', 'error')} ">
	<label for="endTime">
		<g:message code="chatSchedule.endTime.label" default="End Time" />
		
	</label>
	<g:textField name="endTime" value="${chatScheduleInstance?.endTime}" />

</div>


