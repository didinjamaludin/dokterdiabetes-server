
<%@ page import="com.eximiomedia.ChatSchedule" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'chatSchedule.label', default: 'ChatSchedule')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<div class="myth-add">
			<div class="nav nav-myth">
				<div class="left">
					<span class="icon"></span> <span class="text"
						onclick="window.history.back()">Back</span>
				</div>
				<div class="right">
					<div class="btn-blue cursor">
						<span class="icon"></span>
						<g:link action="create">
							<span class="text">add new schedule</span>
						</g:link>
					</div>
				</div>
			</div>
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list chatSchedule">
			
				<g:if test="${chatScheduleInstance?.day}">
				<li class="fieldcontain">
					<span id="day-label" class="property-label"><g:message code="chatSchedule.day.label" default="Day" /></span>
					
						<span class="property-value" aria-labelledby="day-label"><g:fieldValue bean="${chatScheduleInstance}" field="day"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${chatScheduleInstance?.dokter}">
				<li class="fieldcontain">
					<span id="dokter-label" class="property-label"><g:message code="chatSchedule.dokter.label" default="Dokter" /></span>
					
						<span class="property-value" aria-labelledby="dokter-label"><g:link controller="user" action="show" id="${chatScheduleInstance?.dokter?.id}">${chatScheduleInstance?.dokter?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${chatScheduleInstance?.endTime}">
				<li class="fieldcontain">
					<span id="endTime-label" class="property-label"><g:message code="chatSchedule.endTime.label" default="End Time" /></span>
					
						<span class="property-value" aria-labelledby="endTime-label"><g:fieldValue bean="${chatScheduleInstance}" field="endTime"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${chatScheduleInstance?.startTime}">
				<li class="fieldcontain">
					<span id="startTime-label" class="property-label"><g:message code="chatSchedule.startTime.label" default="Start Time" /></span>
					
						<span class="property-value" aria-labelledby="startTime-label"><g:fieldValue bean="${chatScheduleInstance}" field="startTime"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:chatScheduleInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${chatScheduleInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
