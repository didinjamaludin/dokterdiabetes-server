
<%@ page import="com.eximiomedia.ChatSchedule"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'chatSchedule.label', default: 'ChatSchedule')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="myth-list">
		<div class="nav nav-myth">
			<div class="left">
				<span class="text">List Schedule Chat</span>
			</div>
			<div class="right">
				<div class="btn-blue cursor">
					<span class="icon"></span>
					<g:link action="create">
						<span class="text">add new schedule</span>
					</g:link>
				</div>
			</div>
		</div>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<div class="lists">
			<div class="list-item">
				<table>
					<thead>
						<tr>



							<th><g:message code="chatSchedule.dokter.label"
									default="Dokter" /></th>

							<g:sortableColumn property="day"
								title="${message(code: 'chatSchedule.day.label', default: 'Day')}" />

							<g:sortableColumn property="startTime"
								title="${message(code: 'chatSchedule.startTime.label', default: 'Start Time')}" />

							<g:sortableColumn property="endTime"
								title="${message(code: 'chatSchedule.endTime.label', default: 'End Time')}" />



							<th>Action</th>

						</tr>
					</thead>
					<tbody>
						<g:each in="${chatScheduleInstanceList}" status="i"
							var="chatScheduleInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

								<td><g:link action="show" id="${chatScheduleInstance.id}">
										${fieldValue(bean: chatScheduleInstance, field: "dokter")}
									</g:link></td>

								<td>
									${fieldValue(bean: chatScheduleInstance, field: "day")}
								</td>
								<td>
									${fieldValue(bean: chatScheduleInstance, field: "startTime")}
								</td>


								<td>
									${fieldValue(bean: chatScheduleInstance, field: "endTime")}
								</td>



								<td><g:form
										url="[resource:chatScheduleInstance, action:'delete']"
										method="DELETE">
										<g:link class="edit" action="edit"
											resource="${chatScheduleInstance}">
											<g:message code="default.button.edit.label" default="Edit" />
										</g:link>
										<g:actionSubmit class="delete" action="delete"
											value="${message(code: 'default.button.delete.label', default: 'Delete')}"
											onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
									</g:form></td>

							</tr>
						</g:each>
					</tbody>
				</table>
			</div>
		</div>
		<div class="pagination">
			<g:paginate total="${chatScheduleInstanceCount ?: 0}" />
		</div>
	</div>
</body>
</html>
