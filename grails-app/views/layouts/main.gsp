<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title><g:layoutTitle default="Grails" /></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon"
	href="${resource(dir: 'images', file: 'favicon.ico')}"
	type="image/x-icon">
<link rel="apple-touch-icon"
	href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
<link rel="apple-touch-icon" sizes="114x114"
	href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
<link rel="stylesheet"
	href="${resource(dir: 'font-awesome/css', file: 'font-awesome.min.css')}"
	type="text/css">	
<link rel="stylesheet"
	href="${resource(dir: 'assets/css', file: 'style.css')}"
	type="text/css">
<g:javascript library="jquery" />
<r:require module="jquery-ui" />
<ckeditor:resources />
<g:javascript library="template" />
<r:require module="export" />
<export:resource />
<g:layoutHead />
<g:javascript library="application" />
<r:layoutResources />
</head>
<body>
	<div class="header">
		<div class="left">
			<sec:ifAnyGranted roles="ROLE_ADMIN">
				<div class="icon title" onclick="adminhome()"></div>
			</sec:ifAnyGranted>
			<sec:ifAnyGranted roles="ROLE_DOCTOR">
				<div class="icon title" onclick="doctorhome()"></div>
			</sec:ifAnyGranted>
		</div>
		<sec:ifLoggedIn>
			<div class="right">
				<div class="navbar-right">
					<span class="icon username"></span> <span class="text"><sec:loggedInUserInfo
							field="fullname"></sec:loggedInUserInfo></span> <span class="divider"></span>
					<span style="clear: both"></span>
				</div>
				<div class="navbar-right btn-logout" onclick="logout()">
					<span class="icon logout"></span> <span class="text">Log out</span>
					<span class="divider"></span> <span style="clear: both"></span>
				</div>
			</div>
		</sec:ifLoggedIn>
	</div>
	<div class="container" id="maincont">
		<sec:ifLoggedIn>
			<div class="leftbar">
				<div class="menu">
					<sec:ifAnyGranted roles="ROLE_DOCTOR">
						<div class="sidebar-chat">
							<g:link controller="doctor" action="chat">
								<span class="notif" id="chatnotif"></span>
								<span class="icon image"></span>
								<span class="text">Chat</span>
							</g:link>
						</div>
						<div class="sidebar-message">
							<g:link controller="question">
								<div class="notif" id="msgnotif"></div>
								<div class="icon image"></div>
								<div class="text">Pesan</div>
							</g:link>
						</div>
					</sec:ifAnyGranted>
					<sec:ifAnyGranted roles="ROLE_ADMIN">
						<div class="sidebar-article">
							<g:link controller="article" action="index">
								<div class="text"><i class="fa fa-file-text-o"></i>&nbsp;&nbsp;Article</div>
							</g:link>
						</div>
						<div class="sidebar-myth mitfak">
							<g:link controller="mitosfakta" action="index">
								<div class="text"><i class="fa fa-eye-slash"></i>&nbsp;Myth&Fact</div>
							</g:link>
						</div>
						<div class="sidebar-myth cal">
							<g:link controller="calorie" action="index">
								<div class="text"><i class="fa fa-cutlery"></i>&nbsp;&nbsp;Calorie</div>
							</g:link>
						</div>
						<div class="sidebar-myth sched">
							<g:link controller="chatSchedule" action="index">
								<div class="text"><i class="fa fa-calendar"></i>&nbsp;&nbsp;Schedule</div>
							</g:link>
						</div>
						<div class="sidebar-myth quest">
							<g:link controller="question" action="adminquestion">
								<div class="text"><i class="fa fa-question-circle"></i>&nbsp;&nbsp;Questions</div>
							</g:link>
						</div>
						<div class="sidebar-myth news">
							<g:link controller="news" action="index">
								<div class="text"><i class="fa fa-newspaper-o"></i> News</div>
							</g:link>
						</div>
						<div class="sidebar-myth doctor">
							<g:link controller="user" action="index">
								<div class="text"><i class="fa fa-user"></i>&nbsp;&nbsp;&nbsp;Doctor</div>
							</g:link>
						</div>
						<div class="sidebar-myth stat">
							<g:link controller="statistic">
								<div class="text"><i class="fa fa-bar-chart"></i> Statistic</div>
							</g:link>
						</div>
					</sec:ifAnyGranted>
				</div>
			</div>
		</sec:ifLoggedIn>
		<div class="content">
			<g:layoutBody />
		</div>
	</div>
	<script>
		function logout() {
			window.location.href = '${createLink(controller: "logout")}';
		}

		function adminhome() {
			window.location.href = '${createLink(controller: "admin")}';
		}

		function doctorhome() {
			window.location.href = '${createLink(controller: "doctor")}';
		}

		$(function() {
			var pathArray = window.location.pathname.split('/');
			if (pathArray[3] == "chat") {
				$(".sidebar-chat.sidebar-message.sidebar-article.sidebar-myth")
						.removeClass("active");
				$(".sidebar-chat").addClass("active");
			}
			if (pathArray[2] == "question") {
				$(".sidebar-chat.sidebar-message.sidebar-article.sidebar-myth")
						.removeClass("active");
				$(".sidebar-message").addClass("active");
			}
			if (pathArray[2] == "article") {
				$(".sidebar-chat.sidebar-message.sidebar-article.sidebar-myth")
						.removeClass("active");
				$(".sidebar-article").addClass("active");
			}
			if (pathArray[2] == "mitosfakta") {
				$(".sidebar-chat.sidebar-message.sidebar-article.sidebar-myth")
						.removeClass("active");
				$(".mitfak").addClass("active");
			}
			if (pathArray[2] == "calorie") {
				$(".sidebar-chat.sidebar-message.sidebar-article.sidebar-myth")
						.removeClass("active");
				$(".cal").addClass("active");
			}
			if (pathArray[2] == "chatSchedule") {
				$(".sidebar-chat.sidebar-message.sidebar-article.sidebar-myth")
						.removeClass("active");
				$(".sched").addClass("active");
			}
			if (pathArray[2] == "question") {
				$(".sidebar-chat.sidebar-message.sidebar-article.sidebar-myth")
						.removeClass("active");
				$(".quest").addClass("active");
			}
			if (pathArray[2] == "news") {
				$(".sidebar-chat.sidebar-message.sidebar-article.sidebar-myth")
						.removeClass("active");
				$(".news").addClass("active");
			}
			if (pathArray[2] == "user") {
				$(".sidebar-chat.sidebar-message.sidebar-article.sidebar-myth")
						.removeClass("active");
				$(".doctor").addClass("active");
			}
			if (pathArray[2] == "statistic") {
				$(".sidebar-chat.sidebar-message.sidebar-article.sidebar-myth")
						.removeClass("active");
				$(".stat").addClass("active");
			}
		});
	</script>
	<r:layoutResources />
</body>
</html>
