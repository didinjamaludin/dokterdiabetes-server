<%@ page import="com.eximiomedia.News"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'news.label', default: 'News')}" />
<title><g:message code="default.edit.label" args="[entityName]" /></title>
</head>
<body>
	<div class="article-add">
		<div class="nav nav-article">
			<div class="left">
				<span class="icon"></span> <span class="text"
					onclick="window.history.back()">List News</span>
			</div>
			<div class="right">
				<div class="btn-blue cursor">
					<span class="icon"></span>
					<g:link action="create">
						<span class="text">add news</span>
					</g:link>
				</div>
			</div>
		</div>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<g:hasErrors bean="${newsInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${newsInstance}" var="error">
					<li
						<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
							error="${error}" /></li>
				</g:eachError>
			</ul>
		</g:hasErrors>
		<div class="container">
			<g:form url="[resource:newsInstance, action:'update']" method="PUT"
				id="news-update-form">
				<g:hiddenField name="version" value="${newsInstance?.version}" />
				<g:render template="form" />
				<div class="form-button right">
					<div class="calendar">
						<g:datePicker name="publishSchedule" precision="day"
							value="${newsInstance?.publishSchedule}" />
					</div>
					<div class="schedule">
						<span>Schedule</span>
						<g:checkBox name="isSchedule"
							value="${newsInstance?.isSchedule}" />
					</div>
					<div class="btn-submit">
						<div class="btn-blue publish">
							<span class="icon"></span> <span class="text"
								onclick="$('#news-update-form').submit()">Update This
								News</span>
						</div>
					</div>
				</div>
			</g:form>
		</div>
	</div>
</body>
</html>
