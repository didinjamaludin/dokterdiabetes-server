<%@ page import="com.eximiomedia.News" %>

<div class="fieldcontain ${hasErrors(bean: newsInstance, field: 'newsTitle', 'error')} form-title">
	<div class="title nav">
		<span class="left">News Title</span>
	</div>
	<div class="input">
	<g:textField name="newsTitle" value="${newsInstance?.newsTitle}" placeholder="Write news title" />
	</div>

</div>

<div class="fieldcontain ${hasErrors(bean: newsInstance, field: 'newsContent', 'error')} form-textarea">
	<ckeditor:editor name="newsContent" height="100px" width="100%">
		${newsInstance?.newsContent}
	</ckeditor:editor>
</div>

