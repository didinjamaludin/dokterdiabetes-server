
<%@ page import="com.eximiomedia.News"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'news.label', default: 'News')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div id="show-news" class="content scaffold-show" role="main">
		<h1>
			<g:message code="default.show.label" args="[entityName]" />
		</h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<div class="modal-news-content">
			<h3>${newsInstance?.newsTitle}</h3>
		</div>
		<div class="modal-news-content">
			<div class="modal-news-block">
				<div class="modal-news-block-head">
					<h3>Event</h3>
				</div>
				<div class="modal-news-block-content">
					<p>${newsInstance?.newsContent}</p>
				</div>
			</div>
		</div>
		<g:form url="[resource:newsInstance, action:'delete']" method="DELETE">
			<fieldset class="buttons">
				<g:link class="edit" action="edit" resource="${newsInstance}">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:link>
				<g:actionSubmit class="delete" action="delete"
					value="${message(code: 'default.button.delete.label', default: 'Delete')}"
					onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />					
					
			</fieldset>
		</g:form>
	</div>
</body>
</html>
