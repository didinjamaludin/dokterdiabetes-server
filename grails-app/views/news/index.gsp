
<%@ page import="com.eximiomedia.News"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'news.label', default: 'News')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="article-list">
		<div class="nav nav-article">
			<div class="left">
				<span class="text">List News</span>
			</div>
			<div class="right">
				<div class="btn-blue cursor">
					<span class="icon"></span>
					<g:link action="create">
						<span class="text">add news</span>
					</g:link>
				</div>
			</div>
		</div>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<div class="lists">
			<div class="list-item">
				<table>
					<thead>
						<tr>
						<g:sortableColumn property="newsTitle"
								title="${message(code: 'news.newsTitle.label', default: 'News Title')}" />

								<g:sortableColumn property="createDate"
								title="${message(code: 'news.createDate.label', default: 'Create Date')}" />
								
								<th>Action</th>

						</tr>
					</thead>
					<tbody>
						<g:each in="${newsInstanceList}" status="i" var="newsInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

								<td><g:link action="show" id="${newsInstance.id}">
										${newsInstance?.newsTitle?.decodeHTML()}
									</g:link></td>

								<td style="text-align:center;"><g:formatDate date="${newsInstance.createDate}" format="MMMMM dd, yyyy" /></td>

								<td style="text-align:center;"><g:link class="edit" action="edit" resource="${newsInstance}">Edit</g:link></td>
							</tr>
						</g:each>
					</tbody>
				</table>
			</div>
		</div>
		<div class="pagination">
			<g:paginate total="${newsInstanceCount ?: 0}" />
		</div>
	</div>
</body>
</html>
