<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="layout" content="main" />
<title>Insert title here</title>
</head>
<body>
	<div class="myth-list">
		<div class="nav nav-myth">
			<div class="left">
				<span class="text">List Calorie</span>
			</div>
			<div class="right">
				<div class="btn-blue cursor">
					<span class="icon"></span>
					<g:link action="create">
						<span class="text">add new calorie</span>
					</g:link>
				</div>
				<div class="btn-blue cursor">
					<span class="icon"></span>
					<g:link action="importcal">
						<span class="text">Import from excel</span>
					</g:link>
				</div>
			</div>
		</div>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<g:form url="[resource:calorieInstance, action:'uploadCal']"
				enctype="multipart/form-data">
			<input type="file" name="calFile" />
			<g:submitButton name="submit" value="Upload" />
		</g:form>		
	</div>
</body>
</html>