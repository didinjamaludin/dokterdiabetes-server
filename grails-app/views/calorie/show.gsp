
<%@ page import="com.eximiomedia.Calorie"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'calorie.label', default: 'Calorie')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div class="myth-add">
		<div class="nav nav-myth">
			<div class="left">
				<span class="icon"></span> <span class="text"
					onclick="window.history.back()">Back</span>
			</div>
			<div class="right">
				<div class="btn-blue cursor">
					<span class="icon"></span>
					<g:link action="create">
						<span class="text">add new calorie</span>
					</g:link>
				</div>
			</div>
		</div>
		<h1>
			<g:message code="default.show.label" args="[entityName]" />
		</h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<ol class="property-list calorie">

			<g:if test="${calorieInstance?.foodname}">
				<li class="fieldcontain"><span id="foodname-label"
					class="property-label"><g:message
							code="calorie.foodname.label" default="Foodname" /></span> <span
					class="property-value" aria-labelledby="foodname-label"><g:fieldValue
							bean="${calorieInstance}" field="foodname" /></span></li>
			</g:if>

			<g:if test="${calorieInstance?.weight}">
				<li class="fieldcontain"><span id="weight-label"
					class="property-label"><g:message
							code="calorie.weight.label" default="Weight" /></span> <span
					class="property-value" aria-labelledby="weight-label"><g:fieldValue
							bean="${calorieInstance}" field="weight" /></span></li>
			</g:if>
			
			<g:if test="${calorieInstance?.calorie}">
				<li class="fieldcontain"><span id="calorie-label"
					class="property-label"><g:message
							code="calorie.calorie.label" default="Calorie" /></span> <span
					class="property-value" aria-labelledby="calorie-label"><g:fieldValue
							bean="${calorieInstance}" field="calorie" /></span></li>
			</g:if>

		</ol>
		<g:form url="[resource:calorieInstance, action:'delete']"
			method="DELETE">
			<fieldset class="buttons">
				<g:link class="edit" action="edit" resource="${calorieInstance}">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:link>
				<g:actionSubmit class="delete" action="delete"
					value="${message(code: 'default.button.delete.label', default: 'Delete')}"
					onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
			</fieldset>
		</g:form>
	</div>
</body>
</html>
