
<%@ page import="com.eximiomedia.Calorie"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'calorie.label', default: 'Calorie')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="myth-list">
		<div class="nav nav-myth">
			<div class="left">
				<span class="text">List Calorie</span>
			</div>
			<div class="right">
				<div class="btn-blue cursor">
					<span class="icon"></span>
					<g:link action="create">
						<span class="text">add new calorie</span>
					</g:link>
				</div>
				<div class="btn-blue cursor">
					<span class="icon"></span>
					<g:link action="importcal">
						<span class="text">Import from excel</span>
					</g:link>
				</div>
			</div>
		</div>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<div class="lists">
			<div class="list-item">
				<table>
					<thead>
						<tr>
							<g:sortableColumn property="foodname"
								title="${message(code: 'calorie.foodname.label', default: 'Foodname')}" />

							<g:sortableColumn property="weight"
								title="${message(code: 'calorie.weight.label', default: 'Weight')}" />

							<g:sortableColumn property="calorie"
								title="${message(code: 'calorie.calorie.label', default: 'Calorie')}" />

							<th>Action</th>

						</tr>
					</thead>
					<tbody>
						<g:each in="${calorieInstanceList}" status="i"
							var="calorieInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

								<td><g:link action="show" id="${calorieInstance.id}">
										${fieldValue(bean: calorieInstance, field: "foodname")}
									</g:link></td>

								<td>
									${fieldValue(bean: calorieInstance, field: "weight")}
								</td>

								<td>
									${fieldValue(bean: calorieInstance, field: "calorie")}
								</td>

								<td><g:form
										url="[resource:calorieInstance, action:'delete']"
										method="DELETE">
										<g:link class="edit" action="edit"
											resource="${calorieInstance}">
											<g:message code="default.button.edit.label" default="Edit" />
										</g:link>
										<g:actionSubmit class="delete" action="delete"
											value="${message(code: 'default.button.delete.label', default: 'Delete')}"
											onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
									</g:form></td>

							</tr>
						</g:each>
					</tbody>
				</table>
			</div>
		</div>
		<div class="pagination">
			<g:paginate total="${calorieInstanceCount ?: 0}" />
		</div>
	</div>
</body>
</html>
