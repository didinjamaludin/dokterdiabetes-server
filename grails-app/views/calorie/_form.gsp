<%@ page import="com.eximiomedia.Calorie" %>


<div class="fieldcontain ${hasErrors(bean: calorieInstance, field: 'foodname', 'error')} ">
	<label for="foodname">
		<g:message code="calorie.foodname.label" default="Foodname" />
		
	</label>
	<g:textField name="foodname" value="${calorieInstance?.foodname}" />

</div>

<div class="fieldcontain ${hasErrors(bean: calorieInstance, field: 'weight', 'error')} ">
	<label for="weight">
		<g:message code="calorie.weight.label" default="Weight" />
		
	</label>
	<g:field type="number" name="weight" value="${calorieInstance.weight}" />

</div>

<div class="fieldcontain ${hasErrors(bean: calorieInstance, field: 'calorie', 'error')} ">
	<label for="calorie">
		<g:message code="calorie.calorie.label" default="Calorie" />
		
	</label>
	<g:field type="number" name="calorie" value="${calorieInstance.calorie}" />

</div>

