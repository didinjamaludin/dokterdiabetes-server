
<%@ page import="com.eximiomedia.Mitosfakta"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'mitosfakta.label', default: 'Mitosfakta')}" />
<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
	<div class="myth-add">
		<div class="nav nav-myth">
			<div class="left">
				<span class="icon"></span> <span class="text"
					onclick="window.history.back()">Back</span>
			</div>
			<div class="right">
				<div class="btn-blue cursor">
					<span class="icon"></span>
					<g:link action="create">
						<span class="text">add new myth & fact</span>
					</g:link>
				</div>
			</div>
		</div>
		<h1>
			<g:message code="default.show.label" args="[entityName]" />
		</h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<ol class="property-list mitosfakta">

			<g:if test="${mitosfaktaInstance?.mfImage}">
				<li class="fieldcontain"><span id="mfImage-label"
					class="property-label"><g:message
							code="mitosfakta.mfImage.label" default="Mf Image" /></span> <span
					class="property-value" aria-labelledby="mfImage-label"><img src="http://articles.appdokter.com/${mitosfaktaInstance?.mfImage}" /></span></li>
			</g:if>

			<g:if test="${mitosfaktaInstance?.createDate}">
				<li class="fieldcontain"><span id="createDate-label"
					class="property-label"><g:message
							code="mitosfakta.createDate.label" default="Create Date" /></span> <span
					class="property-value" aria-labelledby="createDate-label"><g:formatDate
							date="${mitosfaktaInstance?.createDate}" format="dd/MM/yyyy HH:ss" /></span></li>
			</g:if>
			
			
			<g:if test="${mitosfaktaInstance?.mfTitle}">
				<li class="fieldcontain"><span id="mfTitle-label"
					class="property-label"><g:message
							code="mitosfakta.mfTitle.label" default="Mf Title" /></span> <span
					class="property-value" aria-labelledby="mfTitle-label"><g:fieldValue
							bean="${mitosfaktaInstance}" field="mfTitle" /></span></li>
			</g:if>
			
			<g:if test="${mitosfaktaInstance?.mfDesc}">
				<li class="fieldcontain"><span id="mfDesc-label"
					class="property-label"><g:message
							code="mitosfakta.mfDesc.label" default="Mf Desc" /></span> <span
					class="property-value" aria-labelledby="mfDesc-label"><g:fieldValue
							bean="${mitosfaktaInstance}" field="mfDesc" /></span></li>
			</g:if>
			
			<g:if test="${mitosfaktaInstance?.mfMitfak}">
				<li class="fieldcontain"><span id="mfMitfak-label"
					class="property-label"><g:message
							code="mitosfakta.mfMitfak.label" default="Mf Mitfak" /></span> <span
					class="property-value" aria-labelledby="mfMitfak-label"><g:fieldValue
							bean="${mitosfaktaInstance}" field="mfMitfak" /></span></li>
			</g:if>

			<g:if test="${mitosfaktaInstance?.isSchedule}">
				<li class="fieldcontain"><span id="isSchedule-label"
					class="property-label"><g:message
							code="mitosfakta.isSchedule.label" default="Is Schedule" /></span> <span
					class="property-value" aria-labelledby="isSchedule-label"><g:formatBoolean
							boolean="${mitosfaktaInstance?.isSchedule}" /></span></li>
			</g:if>

			<g:if test="${mitosfaktaInstance?.publishSchedule}">
				<li class="fieldcontain"><span id="publishSchedule-label"
					class="property-label"><g:message
							code="mitosfakta.publishSchedule.label"
							default="Publish Schedule" /></span> <span class="property-value"
					aria-labelledby="publishSchedule-label"><g:formatDate
							date="${mitosfaktaInstance?.publishSchedule}" format="dd/MM/yyyy HH:ss" /></span></li>
			</g:if>

		</ol>
		<g:form url="[resource:mitosfaktaInstance, action:'delete']"
			method="DELETE">
			<fieldset class="buttons">
				<g:link class="edit" action="edit" resource="${mitosfaktaInstance}">
					<g:message code="default.button.edit.label" default="Edit" />
				</g:link>
				<g:actionSubmit class="delete" action="delete"
					value="${message(code: 'default.button.delete.label', default: 'Delete')}"
					onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
			</fieldset>
		</g:form>
	</div>
</body>
</html>
