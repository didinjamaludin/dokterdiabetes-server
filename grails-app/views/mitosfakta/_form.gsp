<%@ page import="com.eximiomedia.Mitosfakta"%>


<div
	class="fieldcontain ${hasErrors(bean: mitosfaktaInstance, field: 'mfTitle', 'error')} ">
	<label for="mfTitle"> <g:message
			code="mitosfakta.mfTitle.label" default="Mf Title" />

	</label>
	<ckeditor:editor name="mfTitle" height="100px" width="100%">
		${mitosfaktaInstance?.mfTitle}
	</ckeditor:editor>

</div>

<div
	class="fieldcontain ${hasErrors(bean: mitosfaktaInstance, field: 'mfDesc', 'error')} ">
	<label for="mfDesc"> <g:message code="mitosfakta.mfDesc.label"
			default="Mf Desc" />

	</label>
	<ckeditor:editor name="mfDesc" height="100px" width="100%">
		${mitosfaktaInstance?.mfDesc}
	</ckeditor:editor>
</div>

<div
	class="fieldcontain ${hasErrors(bean: mitosfaktaInstance, field: 'mfImage', 'error')} ">
	<label for="mfImage"> <g:message
			code="mitosfakta.mfImage.label" default="Mf Image" />

	</label>	
	<input type="file" name="mfImageFile" />
	<g:if test="${params.action.equals('edit')}">
		<g:hiddenField name="mfImage" value="${mitosfaktaInstance?.mfImage}" />
	</g:if>
</div>

<div
	class="fieldcontain ${hasErrors(bean: mitosfaktaInstance, field: 'mfMitfak', 'error')} ">
	<label for="mfMitfak"> <g:message
			code="mitosfakta.mfMitfak.label" default="Mf Mitfak" />

	</label>
	<g:select from="${mitosfaktaInstance?.constraints?.mfMitfak?.inList}" name="mfMitfak" value="${mitosfaktaInstance?.mfMitfak}" />
</div>


<div
	class="fieldcontain ${hasErrors(bean: mitosfaktaInstance, field: 'isSchedule', 'error')} ">
	<label for="isSchedule"> <g:message
			code="mitosfakta.isSchedule.label" default="Is Schedule" />

	</label>
	<g:checkBox name="isSchedule" value="${mitosfaktaInstance?.isSchedule}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: mitosfaktaInstance, field: 'publishSchedule', 'error')} ">
	<label for="publishSchedule"> <g:message
			code="mitosfakta.publishSchedule.label" default="Publish Schedule" />

	</label>
	<g:datePicker name="publishSchedule" precision="day"
		value="${mitosfaktaInstance?.publishSchedule}" />

</div>

