
<%@ page import="com.eximiomedia.Mitosfakta"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'mitosfakta.label', default: 'Mitosfakta')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="myth-list">
		<div class="nav nav-myth">
			<div class="left">
				<span class="text">List Myth and Fact</span>
			</div>
			<div class="right">
				<div class="btn-blue cursor"
					onclick="location.href='<?php echo $base_url; ?>myth/add';">
					<span class="icon"></span>
					<g:link action="create">
						<span class="text">add new myth & fact</span>
					</g:link>
				</div>
			</div>
		</div>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<div class="lists">
			<div class="list-item">
				<table>
					<g:each in="${mitosfaktaInstanceList}" status="i"
						var="mitosfaktaInstance">
						<tr>
							<td class="number">
								${i+1}
							</td>
							<td class="review"><span class="title"> <g:link action="show" id="${mitosfaktaInstance?.id}">${raw(mitosfaktaInstance?.mfTitle)}</g:link>
							</span> <span class="description"> ${raw(mitosfaktaInstance?.mfDesc)}
							</span></td>
							<td class="launch"><span class="schedule">Schedule </span> <span
								class="text">: <g:formatDate
										date="${mitosfaktaInstance?.publishSchedule}"
										format="dd.MM.yy" /></span></td>
							<td class="action">
								<div class="list">
									<g:form url="[resource:mitosfaktaInstance, action:'delete']"
										method="DELETE">
										<div class="btn-blue edit">
											<span class="icon"></span> <span class="text"><g:link
													class="edit" action="edit" resource="${mitosfaktaInstance}">Edit</g:link></span>
										</div>
										<div class="btn-blue delete">
											<span class="icon"></span> <span class="text"><g:actionSubmit
													class="delete" action="delete"
													value="${message(code: 'default.button.delete.label', default: 'Delete')}"
													onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" /></span>
										</div>
									</g:form>
								</div>
							</td>
						</tr>
					</g:each>
				</table>
			</div>
		</div>
		<div class="pagination">
			<g:paginate total="${mitosfaktaInstanceCount ?: 0}" />
		</div>
	</div>
</body>
</html>
