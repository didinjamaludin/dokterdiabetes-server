<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'mitosfakta.label', default: 'Mitosfakta')}" />
<title><g:message code="default.create.label"
		args="[entityName]" /></title>
</head>
<body>
	<div class="myth-add">
		<div class="nav nav-myth">
			<div class="left">
				<span class="icon"></span> <span class="text"
					onclick="window.history.back()">List Myth & Fact</span>
			</div>
			<div class="right">
				<div class="btn-blue cursor">
					<span class="icon"></span>
					<g:link action="create">
						<span class="text">add new myth & fact</span>
					</g:link>
				</div>
			</div>
		</div>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<g:hasErrors bean="${mitosfaktaInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${mitosfaktaInstance}" var="error">
					<li
						<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
							error="${error}" /></li>
				</g:eachError>
			</ul>
		</g:hasErrors>
		<g:form url="[resource:mitosfaktaInstance, action:'save']" enctype="multipart/form-data">
			<fieldset class="form">
				<g:render template="form" />
			</fieldset>
			<fieldset class="buttons">
				<g:submitButton name="create" class="save"
					value="${message(code: 'default.button.create.label', default: 'Create')}" />
			</fieldset>
		</g:form>
	</div>
</body>
</html>
