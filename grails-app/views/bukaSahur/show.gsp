
<%@ page import="com.eximiomedia.BukaSahur" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'bukaSahur.label', default: 'BukaSahur')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-bukaSahur" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-bukaSahur" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list bukaSahur">
			
				<g:if test="${bukaSahurInstance?.notifDate}">
				<li class="fieldcontain">
					<span id="notifDate-label" class="property-label"><g:message code="bukaSahur.notifDate.label" default="Notif Date" /></span>
					
						<span class="property-value" aria-labelledby="notifDate-label"><g:formatDate date="${bukaSahurInstance?.notifDate}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${bukaSahurInstance?.notifMessage}">
				<li class="fieldcontain">
					<span id="notifMessage-label" class="property-label"><g:message code="bukaSahur.notifMessage.label" default="Notif Message" /></span>
					
						<span class="property-value" aria-labelledby="notifMessage-label"><g:fieldValue bean="${bukaSahurInstance}" field="notifMessage"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${bukaSahurInstance?.notifType}">
				<li class="fieldcontain">
					<span id="notifType-label" class="property-label"><g:message code="bukaSahur.notifType.label" default="Notif Type" /></span>
					
						<span class="property-value" aria-labelledby="notifType-label"><g:fieldValue bean="${bukaSahurInstance}" field="notifType"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:bukaSahurInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${bukaSahurInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
