<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'bukaSahur.label', default: 'BukaSahur')}" />
<title><g:message code="default.create.label"
		args="[entityName]" /></title>
</head>
<body>
	<div class="article-add">
		<div class="nav nav-article">
			<div class="left">
				<span class="icon"></span> <span class="text"
					onclick="window.history.back()">List Buka Sahur Notif</span>
			</div>
			<div class="right">
				<div class="btn-blue cursor">
					<span class="icon"></span>
					<g:link action="index">
						<span class="text">back to list</span>
					</g:link>
				</div>
			</div>
		</div>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<g:hasErrors bean="${bukaSahurInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${bukaSahurInstance}" var="error">
					<li
						<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
							error="${error}" /></li>
				</g:eachError>
			</ul>
		</g:hasErrors>
		<div class="container">
			<g:form url="[resource:bukaSahurInstance, action:'save']" id="notif-form">
				<g:render template="form"/>
				<div class="form-button right">
					<div class="btn-blue publish">
					<span class="icon"></span> <span class="text"
								onclick="$('#notif-form').submit()">Save</span>
					</div>
				</div>
			</g:form>
		</div>
	</div>
</body>
</html>
