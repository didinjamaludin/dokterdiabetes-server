<%@ page import="com.eximiomedia.BukaSahur"%>



<div
	class="fieldcontain ${hasErrors(bean: bukaSahurInstance, field: 'notifDate', 'error')} form-title">
	<div class="title nav">
		<span class="left">Notif Date</span>
	</div>
	<div class="input">
		<g:datePicker name="notifDate" precision="day"
			value="${bukaSahurInstance?.notifDate}" />
	</div>
</div>

<div
	class="fieldcontain ${hasErrors(bean: bukaSahurInstance, field: 'notifMessage', 'error')} form-textarea">
	<ckeditor:editor name="notifMessage" height="100px" width="100%">
		${bukaSahurInstance?.notifMessage}
	</ckeditor:editor>
</div>

<div
	class="fieldcontain ${hasErrors(bean: bukaSahurInstance, field: 'notifType', 'error')} form-title">
	<div class="title nav">
		<span class="left">Notif Type</span>
	</div>
	<div class="input">
		<g:select from="${bukaSahurInstance?.constraints?.notifType?.inList}"
			value="${incidentInstance?.state}" name="notifType" />
	</div>
</div>

