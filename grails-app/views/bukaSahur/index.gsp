
<%@ page import="com.eximiomedia.BukaSahur"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'bukaSahur.label', default: 'BukaSahur')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="article-list">
		<div class="nav nav-article">
			<div class="left">
				<span class="text">List Buka Sahur Notif</span>
			</div>
			<div class="right">
				<div class="btn-blue cursor">
					<span class="icon"></span>
					<g:link action="create">
						<span class="text">add new notif</span>
					</g:link>
				</div>
			</div>
		</div>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<div class="lists">
			<div class="list-item">
				<table>
					<thead>
						<tr>

							<g:sortableColumn property="notifDate"
								title="${message(code: 'bukaSahur.notifDate.label', default: 'Notif Date')}" />

							<g:sortableColumn property="notifMessage"
								title="${message(code: 'bukaSahur.notifMessage.label', default: 'Notif Message')}" />

							<g:sortableColumn property="notifType"
								title="${message(code: 'bukaSahur.notifType.label', default: 'Notif Type')}" />

						</tr>
					</thead>
					<tbody>
						<g:each in="${bukaSahurInstanceList}" status="i"
							var="bukaSahurInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

								<td><g:link action="show" id="${bukaSahurInstance.id}">
										<g:formatDate date="${bukaSahurInstance?.notifDate}" format="dd/MM/yy" />
									</g:link></td>

								<td>
									${bukaSahurInstance?.notifMessage.decodeHTML()}
								</td>

								<td>
									${fieldValue(bean: bukaSahurInstance, field: "notifType")}
								</td>

							</tr>
						</g:each>
					</tbody>
				</table>
			</div>
		</div>
		<div class="pagination">
			<g:paginate total="${bukaSahurInstanceCount ?: 0}" />
		</div>
	</div>
</body>
</html>
