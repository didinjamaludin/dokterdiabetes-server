<%@ page import="com.eximiomedia.AppVersion" %>



<div class="fieldcontain ${hasErrors(bean: appVersionInstance, field: 'appversion', 'error')} ">
	<label for="appversion">
		<g:message code="appVersion.appversion.label" default="Appversion" />
		
	</label>
	<g:textField name="appversion" value="${appVersionInstance?.appversion}" />

</div>

