
<g:each in="${chatInstanceList}" var="chatInstance" status="cs">
	<div class="details-item">
		<div class="picture">
			<g:if test="${chatInstance?.sender.equals('doctor')}">
				<g:img class="profile-picture" dir="images" file="${chatInstance?.doctor?.photo}" />
			</g:if>
			<g:if test="${chatInstance?.sender.equals('user')}">
				<g:img class="profile-picture" dir="images" file="profile.png" />
			</g:if>
		</div>
		<div class="chat">
			<span class="profile-detail"> <span class="name left">
					<g:if test="${chatInstance?.sender.equals('user')}">
						${chatInstance?.user?.nama}
					</g:if>
					<g:if test="${chatInstance?.sender.equals('doctor')}">
						${chatInstance?.doctor?.fullname}
					</g:if>
			</span> <span class="datetime right"><g:formatDate
						date="${chatInstance?.sendDate}" format="dd.MM.yy - HH:mm:ss" /></span>
			</span> <span class="detail"> ${chatInstance?.message}
			</span>
		</div>
	</div>
</g:each>
