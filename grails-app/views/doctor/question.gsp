<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="layout" content="main" />
<title>DokterDiabetes - Question</title>
</head>
<body>
	<div class="message-list">
		<div class="search">
			<input type="text" placeholder="Search..">
		</div>
		<div class="list-container">

			<div class="lists" id="questionUserList"></div>
		</div>
	</div>

	<div class="message-detail">
		<div class="nav nav-message">
			<div class="left">
				<span class="username" id="chat-user"></span>
			</div>
			<div class="right"></div>
		</div>

		<div class="message-container">
			<div class="message-container-detail">
				<div class="details" id="questionMsgList"></div>
			</div>
			<div class="reply-container">
				<div class="reply">
					<g:form action="chatReply" enctype="multipart/form-data"
						id="reply-form">
						<g:hiddenField name="sender" value="doctor" id="sender" />
						<textarea class="reply-box" placeholder="Write a reply ..."
							name="message" id="reply-msg"></textarea>
						<div class="reply-nav">
							<div class="left">
								<span class="icon"></span> <span class="text">Add Photo</span> <span
									class="divider"></span>
							</div>
							<div class="right">
								<span class="text">Press Enter to Send</span> <span
									class="checkbox"><input type="checkbox"></span> <span
									class="button"> <span class="icon"></span> <span
									class="text">REPLY</span>
								</span>
							</div>
						</div>
					</g:form>
				</div>

			</div>
		</div>
	</div>

	<script>
		var uid;
	
		$(document).ready(function() {
			$('#reply-msg').keydown(function(e) {
			    if (e.keyCode == 13) {
				    var sender = $("#sender").val();
				    var msg = $("#reply-msg").val();
			    	$.ajax({
				        type: "POST",
				        url: "${createLink(action: 'answer')}",
						data : {
							sender : sender,
							user : uid,
							message : msg
						},
						//processData: false,
						dataType : "json"
					}).done(function(data) {
						showMessageList($("#chat-user").val());
						$("#chat-user").val("");
					}).fail(function(jqXHR, textStatus) {
						alert(textStatus);
						return;
					});
				}
			});
		});

		function chatUserList() {
			<g:remoteFunction action="chatUserList" update="chatUserList" method="GET"/>
		}

		function pollChatUser() {
			chatUserList();
			setTimeout('pollChatUser()', 5000);
		}

		pollChatUser();
	</script>
</body>
</html>