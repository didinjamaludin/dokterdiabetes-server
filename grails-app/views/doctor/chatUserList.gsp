<%@ page import="com.eximiomedia.Chat"%>

<g:each in="${chatUserList.sort{a,b-> a.id.compareTo(b.id)}}" var="chatUser" status="us">
	<div class="list-item active">
		<div class="picture">
			<g:if test="${chatUser?.photo}">
				<img class="profile-picture"
					src="http://articles.appdokter.com/${chatUser?.photo}" />
			</g:if>
			<g:else>
				<g:img class="profile-picture" dir="assets/img" file="profile.png" />
			</g:else>
		</div>
		<div class="list-preview">
			<a href="#" onclick="showMessageList(${chatUser.id});changeStatus(${chatUser.id});"> <span
				class="name"> ${chatUser?.nama}
			</span> <span class="datetime"> <g:formatDate date="${Chat.findAllByUser(chatUser,[max:1,sort:"sendDate",order:"desc"]).sendDate.getAt(0)}" format="dd.MM.yy - HH:mm:ss" />
			</span> <span class="chat-preview">${chatUser?.chatStatus?.status}</span>
			</a>
		</div>
	</div>
</g:each>

<script>
	var chatcount = ${chatUserCount};
	$("#chatnotif").text(chatcount);
</script>