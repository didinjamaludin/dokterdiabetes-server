<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="layout" content="main" />
<title>Doctor Dashboard</title>
</head>
<body>
	<h1>Welcome Doctor</h1>
	<g:if test="${flash.message}">
		<div class="message" role="status">
			${flash.message}
		</div>
	</g:if>
	<table>
		<tbody>
			<tr>
				<td colspan=2><g:link action="chatStartNotif">
						<button name="chatStart">Chat Start Notification</button>
					</g:link> <g:link action="chatStopNotif">
						<button name="chatStop">Chat Stop Notification</button>
					</g:link></td>
			</tr>
			<tr>
				<td colspan="2"><h2>Questions</h2></td>
			</tr>
			<tr>
				<td>Total Question:</td>
				<td>${totquest}</td>
			</tr>
			<tr>
				<td>Total Answered:</td>
				<td>${ansquest}</td>
			</tr>
			<tr>
				<td>Total Not Answered:</td>
				<td>${notansquest}</td>
			</tr>
		</tbody>
	</table>
</body>
</html>