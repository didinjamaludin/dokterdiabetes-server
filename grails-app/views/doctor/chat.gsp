<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="layout" content="main" />
<title>DokterDiabetes - Chat</title>
</head>
<body>
	<div class="chat-list">
		<div class="search">
			<input type="text" placeholder="Search..">
		</div>
		<div class="list-container">

			<div class="lists" id="chatUserList"></div>
		</div>
	</div>

	<div class="chat-detail">
		<div class="nav nav-chat">
			<div class="left">
				<span class="username" id="chat-user"></span>
			</div>
			<div class="right"></div>
		</div>

		<div class="chat-container">
			<div class="chat-container-detail">
				<div class="details" id="chatMessageList" style="min-width:500px"></div>
			</div>
			<div class="reply-container">
				<div class="reply">
					<g:form action="chatReply" enctype="multipart/form-data"
						id="reply-form">
						<g:hiddenField name="sender" value="doctor" id="sender" />
						<textarea class="reply-box" placeholder="Write a reply ..."
							name="message" id="reply-msg" onkeyup="changeStatus2()"></textarea>
						<div class="reply-nav">
							<div class="left">
								<span class="icon"></span> <span class="text">Add Photo</span> <span
									class="divider"></span>
							</div>
							<div class="right">
								<span class="text">Press Enter to Send</span> <span
									class="checkbox"><input type="checkbox"></span> <span
									class="button"> <span class="icon"></span> <span
									class="text">REPLY</span>
								</span>
							</div>
						</div>
					</g:form>
				</div>

			</div>
		</div>
	</div>

	<script>
		var uid;
	
		$(document).ready(function() {
			$('#reply-msg').keydown(function(e) {
			    if (e.keyCode == 13) {
				    var sender = $("#sender").val();
				    var msg = $("#reply-msg").val();
				    var lastchat = $("#lastchat").val();
			    	$.ajax({
				        type: "POST",
				        url: "${createLink(action: 'chatReply')}",
						data : {
							sender : sender,
							user : uid,
							message : msg,
							lastchat : lastchat,
							status : "reply"
						},
						//processData: false,
						dataType : "json"
					}).done(function(data) {
						if(data.movechat=='question') {
							alert("chating telah pindah ke offline");
						}
						showMessageList(uid);
						$("#reply-msg").val("");
						$("#reply-msg").focus();
					}).fail(function(jqXHR, textStatus) {
						alert(textStatus);
						return;
					});
				}
			});
		});

		function changeStatus2() {
			<g:remoteFunction action="changeStatus2" params="\'userid=\'+uid" method="POST"/>
		}

		function changeStatus(userid) {
			<g:remoteFunction action="changeStatus" params="\'userid=\'+userid" method="POST"/>
		}

		function chatUserList() {
			<g:remoteFunction action="chatUserList" update="chatUserList" method="GET"/>
		}

		function pollChatUser() {
			chatUserList();
			setTimeout('pollChatUser()', 5000);
		}

		pollChatUser();

		function showMessageList(userid) {
			uid=userid;
			showMessages(userid);
		}

		function showMessages(userid) {	
			<g:remoteFunction action="chatMessageList" update="chatMessageList" params="\'userid=\'+userid" method="GET"/>
			$("#chatMessageList").animate({ scrollTop: $("#chatMessageList").prop('scrollHeight') }, "slow");
		}

		function pollMessages() {
			if(uid) {
				showMessages(uid);
			}

			setTimeout('pollMessages()', 5000);	
		}

		pollMessages();
	</script>
</body>
</html>