<%@ page import="com.eximiomedia.Chat"%>

<g:each in="${chatUserList}" var="chatUser" status="us">
	<div class="list-item active">
		<div class="picture">
			<g:img class="profile-picture" dir="assets/img" file="profile.png" />
		</div>
		<div class="list-preview">
			<a href="#" onclick="showMessageList(${chatUser.id})" >
				<span class="name"> ${chatUser?.nama}
				</span>
				<span class="datetime">
					${Chat.findByUser(chatUser).sendDate}
				</span>
				<span class="chat-preview"></span>
			</a>
		</div>
	</div>
</g:each>

<script>
	function showMessageList(userid) {
		uid=userid;
		showMessages(userid);
	}

	function showMessages(userid) {	
		<g:remoteFunction action="chatMessageList" update="chatMessageList" params="\'userid=\'+userid" method="GET"/>
	}

	function pollMessages() {
		if(uid) {
			showMessages(uid);
		}
		setTimeout('pollMessages()', 10000);	
	}

	pollMessages();
</script>