<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="layout" content="main" />
<title>Insert title here</title>
</head>
<body>
	<div class="message-list">
		<div class="search">
			<input type="text" placeholder="Search..">
		</div>
		<div class="list-container">

			<div class="lists">

				<div class="list-item active">
					<div class="picture">
						<img class="profile-picture"
							src="<?php echo $base_url; ?>assets/img/profile.png" />
					</div>
					<div class="list-preview">
						<span class="name">Christine Lie</span> <span class="datetime">12.05.14
							- 14:20:30</span> <span class="message-preview">Lorem Ipsum is
							simply dummy te ...</span>
					</div>
				</div>
				<div class="list-item new">
					<span class="icon-new"></span>
					<div class="picture">
						<img class="profile-picture"
							src="<?php echo $base_url; ?>assets/img/profile.png" />
					</div>
					<div class="list-preview">
						<span class="name">Christine Lie</span> <span class="datetime">12.05.14
							- 14:20:30</span> <span class="message-preview">Lorem Ipsum is
							simply dummy te ...</span>
					</div>
				</div>
				<div class="list-item new">
					<span class="icon-new"></span>
					<div class="picture">
						<img class="profile-picture"
							src="<?php echo $base_url; ?>assets/img/profile.png" />
					</div>
					<div class="list-preview">
						<span class="name">Christine Lie</span> <span class="datetime">12.05.14
							- 14:20:30</span> <span class="message-preview">Lorem Ipsum is
							simply dummy te ...</span>
					</div>
				</div>
				<div class="list-item new">
					<span class="icon-new"></span>
					<div class="picture">
						<img class="profile-picture"
							src="<?php echo $base_url; ?>assets/img/profile.png" />
					</div>
					<div class="list-preview">
						<span class="name">Christine Lie</span> <span class="datetime">12.05.14
							- 14:20:30</span> <span class="message-preview">Lorem Ipsum is
							simply dummy te ...</span>
					</div>
				</div>
				<div class="list-item">
					<div class="picture">
						<img class="profile-picture"
							src="<?php echo $base_url; ?>assets/img/profile.png" />
					</div>
					<div class="list-preview">
						<span class="name">Christine Lie</span> <span class="datetime">12.05.14
							- 14:20:30</span> <span class="message-preview">Lorem Ipsum is
							simply dummy te ...</span>
					</div>
				</div>
				<div class="list-item">
					<div class="picture">
						<img class="profile-picture"
							src="<?php echo $base_url; ?>assets/img/profile.png" />
					</div>
					<div class="list-preview">
						<span class="name">Christine Lie</span> <span class="datetime">12.05.14
							- 14:20:30</span> <span class="message-preview">Lorem Ipsum is
							simply dummy te ...</span>
					</div>
				</div>
				<div class="list-item">
					<div class="picture">
						<img class="profile-picture"
							src="<?php echo $base_url; ?>assets/img/profile.png" />
					</div>
					<div class="list-preview">
						<span class="name">Christine Lie</span> <span class="datetime">12.05.14
							- 14:20:30</span> <span class="message-preview">Lorem Ipsum is
							simply dummy te ...</span>
					</div>
				</div>
				<div class="list-item">
					<div class="picture">
						<img class="profile-picture"
							src="<?php echo $base_url; ?>assets/img/profile.png" />
					</div>
					<div class="list-preview">
						<span class="name">Christine Lie</span> <span class="datetime">12.05.14
							- 14:20:30</span> <span class="message-preview">Lorem Ipsum is
							simply dummy te ...</span>
					</div>
				</div>
				<div class="list-item">
					<div class="picture">
						<img class="profile-picture"
							src="<?php echo $base_url; ?>assets/img/profile.png" />
					</div>
					<div class="list-preview">
						<span class="name">Christine Lie</span> <span class="datetime">12.05.14
							- 14:20:30</span> <span class="message-preview">Lorem Ipsum is
							simply dummy te ...</span>
					</div>
				</div>

			</div>
		</div>
	</div>

	<!-- message detail -->
	<div class="message-detail">
		<div class="nav-message">
			<div class="left">
				<span class="username">Christine Lie</span>
			</div>
			<div class="right">
				<div class="btn-detail">
					<span class="icon"></span> <span class="text">Health Detail</span>
				</div>
				<div class="btn-action">
					<span class="icon"></span> <span class="text">Action</span>
				</div>
			</div>
		</div>
		<div class="message-container">
			<div class="message-container-detail">
				<div class="details">

					<div class="details-item">
						<div class="picture">
							<img class="profile-picture"
								src="<?php echo $base_url; ?>assets/img/profile.png" />
						</div>
						<div class="message">
							<span class="profile-detail"> <span class="name left">Christine
									Lie</span> <span class="datetime right">12.05.14 - 14:20:30</span>
							</span> <span class="detail">Pellentesque habitant morbi
								tristique senectus et netus et malesuada fames ac turpis
								egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget,
								tempor sit amet, ante. Donec eu libero sit amet quam egestas
								semper. Aenean ultricies mi vitae est. Mauris placerat eleifend
								leo.</span>
						</div>
					</div>
					<div class="details-item">
						<div class="picture">
							<img class="profile-picture"
								src="<?php echo $base_url; ?>assets/img/profile.png" />
						</div>
						<div class="message">
							<span class="profile-detail"> <span class="name left">Christine
									Lie</span> <span class="datetime right">12.05.14 - 14:20:30</span>
							</span> <span class="detail">Pellentesque habitant morbi
								tristique senectus et netus et malesuada fames ac turpis
								egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget,
								tempor sit amet, ante. Donec eu libero sit amet quam egestas
								semper. Aenean ultricies mi vitae est. Mauris placerat eleifend
								leo.</span>
						</div>
					</div>
					<div class="details-item">
						<div class="picture">
							<img class="profile-picture"
								src="<?php echo $base_url; ?>assets/img/profile.png" />
						</div>
						<div class="message">
							<span class="profile-detail"> <span class="name left">Christine
									Lie</span> <span class="datetime right">12.05.14 - 14:20:30</span>
							</span> <span class="detail">Pellentesque habitant morbi
								tristique senectus et netus et malesuada fames ac turpis
								egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget,
								tempor sit amet, ante. Donec eu libero sit amet quam egestas
								semper. Aenean ultricies mi vitae est. Mauris placerat eleifend
								leo.</span>
						</div>
					</div>
					<div class="details-item">
						<div class="picture">
							<img class="profile-picture"
								src="<?php echo $base_url; ?>assets/img/profile.png" />
						</div>
						<div class="message">
							<span class="profile-detail"> <span class="name left">Christine
									Lie</span> <span class="datetime right">12.05.14 - 14:20:30</span>
							</span> <span class="detail">Pellentesque habitant morbi
								tristique senectus et netus et malesuada fames ac turpis
								egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget,
								tempor sit amet, ante. Donec eu libero sit amet quam egestas
								semper. Aenean ultricies mi vitae est. Mauris placerat eleifend
								leo.</span>
						</div>
					</div>
					<div class="details-item">
						<div class="picture">
							<img class="profile-picture"
								src="<?php echo $base_url; ?>assets/img/profile.png" />
						</div>
						<div class="message">
							<span class="profile-detail"> <span class="name left">Christine
									Lie</span> <span class="datetime right">12.05.14 - 14:20:30</span>
							</span> <span class="detail">Pellentesque habitant morbi
								tristique senectus et netus et malesuada fames ac turpis
								egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget,
								tempor sit amet, ante. Donec eu libero sit amet quam egestas
								semper. Aenean ultricies mi vitae est. Mauris placerat eleifend
								leo.</span>
						</div>
					</div>
					<div class="details-item">
						<div class="picture">
							<img class="profile-picture"
								src="<?php echo $base_url; ?>assets/img/profile.png" />
						</div>
						<div class="message">
							<span class="profile-detail"> <span class="name left">Christine
									Lie</span> <span class="datetime right">12.05.14 - 14:20:30</span>
							</span> <span class="detail">Pellentesque habitant morbi
								tristique senectus et netus et malesuada fames ac turpis
								egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget,
								tempor sit amet, ante. Donec eu libero sit amet quam egestas
								semper. Aenean ultricies mi vitae est. Mauris placerat eleifend
								leo.</span>
						</div>
					</div>
					<div class="details-item">
						<div class="picture">
							<img class="profile-picture"
								src="<?php echo $base_url; ?>assets/img/profile.png" />
						</div>
						<div class="message">
							<span class="profile-detail"> <span class="name left">Christine
									Lie</span> <span class="datetime right">12.05.14 - 14:20:30</span>
							</span> <span class="detail">Pellentesque habitant morbi
								tristique senectus et netus et malesuada fames ac turpis
								egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget,
								tempor sit amet, ante. Donec eu libero sit amet quam egestas
								semper. Aenean ultricies mi vitae est. Mauris placerat eleifend
								leo.</span>
						</div>
					</div>

					<div class="reply-container">
						<div class="reply">
							<textarea class="reply-box" placeholder="Write a reply ..."></textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="reply-container">
				<div class="reply">
					<textarea class="reply-box" placeholder="Write a reply ..."></textarea>
					<div class="reply-nav">
						<div class="left">
							<span class="icon"></span> <span class="text">Add Photo</span> <span
								class="divider"></span>
						</div>
						<div class="right">
							<span class="text">Press Enter to Send</span> <span
								class="checkbox"><input type="checkbox"></span> <span
								class="button"> <span class="icon"></span> <span
								class="text">REPLY</span>
							</span>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</body>
</html>