<%@ page contentType="text/html;charset=UTF-8"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="layout" content="main" />
<title>Dokter Diabetes - Admin Dashboard</title>
</head>
<body>
	<div style="padding: 0 10px">
		<h1>Admin Dashboard</h1>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<h3>Test Push Notification</h3>
		<table style="width:700px;">
			<tr>
				<th>Android</th>
				<th>iOS</th>
			</tr>
			<tr>
				<td width="50%" style="text-align:center">
					<g:form action="sendMessage">
						<g:textArea name="msgTitle" placeholder="title" style="width:300px;border:solid 1px rgb(237,49,59);margin-bottom:10px;" /><br>
						<g:textArea name="msgText" placeholder="message" style="width:300px;border:solid 1px rgb(237,49,59);margin-bottom:10px;" /><br>
						<g:radio name="sendAll" value="all" style="margin-bottom:10px;"/>&nbsp;All&nbsp;&nbsp;&nbsp;
						<g:radio name="sendAll" value="member" style="margin-bottom:10px;"/>&nbsp;Member&nbsp;&nbsp;&nbsp;
						<g:radio name="sendAll" value="nonmember" style="margin-bottom:10px;"/>&nbsp;Non Member<br>
						<g:textField name="getName" id="getName" placeholder="Choose name" style="width:290px;border:solid 1px rgb(237,49,59);margin-bottom:10px;padding:5px;" /><br>
						<ul id="rcvlist"></ul>
						<g:hiddenField name="rows" />
						<g:submitButton name="submit" value="Send" style="color:#fff;background-color:rgb(237,49,59);border:solid 1px rgb(227,29,39);padding: 5px 20px;" />
					</g:form>
				</td>
				<td width="50%" style="text-align:center">
					<g:form action="sendMessageToDevices">
						<g:textArea name="msgTitle2" placeholder="title" style="width:300px;border:solid 1px rgb(237,49,59);margin-bottom:10px;" /><br>
						<g:textArea name="msgText2" placeholder="message" style="width:300px;border:solid 1px rgb(237,49,59);margin-bottom:10px;" /><br>
						<g:radio name="sendAll2" value="all2" style="margin-bottom:10px;"/>&nbsp;All&nbsp;&nbsp;&nbsp;
						<g:radio name="sendAll2" value="member2" style="margin-bottom:10px;"/>&nbsp;Member&nbsp;&nbsp;&nbsp;
						<g:radio name="sendAll2" value="nonmember2" style="margin-bottom:10px;"/>&nbsp;Non Member<br>
						<g:textField name="getName2" id="getName2" placeholder="Choose name" style="width:290px;border:solid 1px rgb(237,49,59);margin-bottom:10px;padding:5px;" /><br>
						<ul id="rcvlist2"></ul>
						<g:hiddenField name="rows2" />
						<g:submitButton name="submit2" value="Send" style="color:#fff;background-color:rgb(237,49,59);border:solid 1px rgb(227,29,39);padding: 5px 20px;" />
					</g:form>
				</td>
			</tr>
			<tr>
				<th colspan="2" style="text-align:left;border-top:solid 1px #777;padding-top:10px;">Apps Version</th>
			</tr>
			<tr>
				<td colspan="2">Version Number : 
					<g:if test="${appVersionInstance}">
						<g:form url="[resource:appVersionInstance.getAt(0), action:'update']" method="PUT">
							<g:textField name="appversion" value="${appVersionInstance?.appversion?.getAt(0)}" style="width:50px;border:solid 1px rgb(237,49,59);"/>
							<g:actionSubmit class="save" action="update" value="Update Version" style="color:#fff;background-color:rgb(237,49,59);border:solid 1px rgb(227,29,39);padding: 5px 20px;" />
						</g:form>
					</g:if>
					<g:else>
						<g:form url="[resource:appVersionInstance, action:'saveAppVersion']" >
							<g:textField name="appversion" value="${appVersionInstance?.appversion}" style="width:50px;border:solid 1px rgb(237,49,59);"/>
							<g:submitButton name="create" class="save" value="Save Version" style="color:#fff;background-color:rgb(237,49,59);border:solid 1px rgb(227,29,39);padding: 5px 20px;" />
						</g:form>
					</g:else>
				</td>
			</tr>
		</table>
		
	</div>
	<script>
		$(window).ready(function() {
			$("#getName").autocomplete({
				  source: function(request, response){
				   $.ajax({
				    url: '<g:createLink controller='admin' action='getUser'/>', // remote datasource
				    data: request,
				    success: function(data){
				     response(data); // set the response
				    }
				   });
				  },
				  select: function(event, ui) { 
				   var rows = $("#rcvlist li").length;	  
				   $("#rcvlist").append('<li><input type="hidden" name="user['+rows+']" value="'+ui.item.id+'" />'+ui.item.label+'</li>');  
				   $("#rows").val($("#rcvlist li").length);
				  }
				 });

			$("#getName2").autocomplete({
				  source: function(request, response){
				   $.ajax({
				    url: '<g:createLink controller='admin' action='getUser'/>', // remote datasource
				    data: request,
				    success: function(data){
				     response(data); // set the response
				    }
				   });
				  },
				  select: function(event, ui) { 
				   var rows = $("#rcvlist2 li").length;	  
				   $("#rcvlist2").append('<li><input type="hidden" name="user2['+rows+']" value="'+ui.item.id+'" />'+ui.item.label+'</li>');  
				   $("#rows2").val($("#rcvlist2 li").length);
				  }
				 });  
		});
	</script>
</body>
</html>