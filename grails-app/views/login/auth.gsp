<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Dokter Diabetes Login</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon"
	href="${resource(dir: 'images', file: 'favicon.ico')}"
	type="image/x-icon">
<link rel="apple-touch-icon"
	href="${resource(dir: 'images', file: 'apple-touch-icon.png')}">
<link rel="apple-touch-icon" sizes="114x114"
	href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}">
<link rel="stylesheet"
	href="${resource(dir: 'assets/css', file: 'login.css')}"
	type="text/css">
<g:javascript library="jquery" />
<r:layoutResources />
</head>
<body>
	<div class="container">
		<div class="login">

			<div class="title-login">DASHBOARD LOGIN</div>
			<div class="form-login">
				<div class="header"></div>
				<g:if test='${flash.message}'>
					<div class="alert alert-warning"
						style="max-width: 330px !important; margin: 0 auto !important;">
						<a href="#" class="close" data-dismiss="alert">&times;</a>
						${flash.message}
					</div>
				</g:if>
				<form action='${postUrl}' method='POST'>
					<div>
						<input name="j_username" type="email" class="field_email" placeholder="Email" />
					</div>
					<div>
						<input name="j_password" type="password"
							class="field_password" placeholder="Password" />
					</div>
					<div>
						<input name="submit" type="submit" class="submit" value="Login" />
					</div>
				</form>
			</div>
		</div>
	</div>
	<r:layoutResources />
</body>
</html>
