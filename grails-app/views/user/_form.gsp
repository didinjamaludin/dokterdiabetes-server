<%@ page import="com.eximiomedia.auth.User"%>


<div style="display: none">
	<div
		class="fieldcontain ${hasErrors(bean: userInstance, field: 'accountExpired', 'error')} ">
		<label for="accountExpired"> <g:message
				code="user.accountExpired.label" default="Account Expired" />

		</label>
		<g:checkBox name="accountExpired"
			value="${userInstance?.accountExpired}" />

	</div>

	<div
		class="fieldcontain ${hasErrors(bean: userInstance, field: 'accountLocked', 'error')} ">
		<label for="accountLocked"> <g:message
				code="user.accountLocked.label" default="Account Locked" />

		</label>
		<g:checkBox name="accountLocked"
			value="${userInstance?.accountLocked}" />

	</div>

	<div
		class="fieldcontain ${hasErrors(bean: userInstance, field: 'createdate', 'error')} ">
		<label for="createdate"> <g:message
				code="user.createdate.label" default="Createdate" />

		</label>
		<g:datePicker name="createdate" precision="day"
			value="${userInstance?.createdate}" />

	</div>

	<div
		class="fieldcontain ${hasErrors(bean: userInstance, field: 'enabled', 'error')} ">
		<label for="enabled"> <g:message code="user.enabled.label"
				default="Enabled" />

		</label>
		<g:checkBox name="enabled" value="${userInstance?.enabled}" />

	</div>

	<div
		class="fieldcontain ${hasErrors(bean: userInstance, field: 'passwordExpired', 'error')} ">
		<label for="passwordExpired"> <g:message
				code="user.passwordExpired.label" default="Password Expired" />

		</label>
		<g:checkBox name="passwordExpired"
			value="${userInstance?.passwordExpired}" />

	</div>

</div>

<div
	class="fieldcontain ${hasErrors(bean: userInstance, field: 'username', 'error')} ">
	<label for="username"> <g:message code="user.username.label"
			default="Username" />

	</label>
	<g:textField name="username" value="${userInstance?.username}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: userInstance, field: 'password', 'error')} ">
	<label for="password"> <g:message code="user.password.label"
			default="Password" />

	</label>
	<g:field type="password" name="password"
		value="${userInstance?.password}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: userInstance, field: 'fullname', 'error')} ">
	<label for="fullname"> <g:message code="user.fullname.label"
			default="Fullname" />

	</label>
	<g:textField name="fullname" value="${userInstance?.fullname}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: userInstance, field: 'specialist', 'error')} ">
	<label for="specialist"> <g:message
			code="user.specialist.label" default="Specialist" />

	</label>
	<g:textField name="specialist" value="${userInstance?.specialist}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: userInstance, field: 'type', 'error')} ">
	<label for="type"> <g:message code="user.type.label"
			default="Type" />

	</label>
	<g:select name="type" from="${['fixed','guest']}"
		value="${userInstance?.type}" />
</div>

<div
	class="fieldcontain ${hasErrors(bean: userInstance, field: 'hospital', 'error')} ">
	<label for="hospital"> <g:message code="user.hospital.label"
			default="Hospital" />

	</label>
	<g:textField name="hospital" value="${userInstance?.hospital}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: userInstance, field: 'notifEmail', 'error')} ">
	<label for="notifEmail"> <g:message
			code="user.notifEmail.label" default="Notif Emails" />

	</label>
	<g:textField name="notifEmail" value="${userInstance?.notifEmail}" />

</div>

<div
	class="fieldcontain ${hasErrors(bean: userInstance, field: 'photo', 'error')} ">
	<label for="photo"> <g:message code="user.photo.label"
			default="Photo" />
	</label> 
	<input type="file" name="photoFile" />
	<g:if test="${params.action.equals('edit')}">
		<g:hiddenField name="photo" value="${userInstance?.photo}" />
	</g:if>
</div>



