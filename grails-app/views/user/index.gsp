
<%@ page import="com.eximiomedia.auth.User"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'user.label', default: 'User')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
	<div class="article-list">
		<div class="nav nav-article">
			<div class="left">
				<span class="text">List Doctor</span>
			</div>
			<div class="right">
				<div class="btn-blue cursor">
					<span class="icon"></span>
					<g:link action="create">
						<span class="text">add new doctor</span>
					</g:link>
				</div>
			</div>
		</div>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<div class="lists">
			<div class="list-item">
				<table style="width:98%">
					<thead>
						<tr>

							<g:sortableColumn property="username"
								title="${message(code: 'user.username.label', default: 'Username')}" />

							<g:sortableColumn property="fullname"
								title="${message(code: 'user.fullname.label', default: 'Fullname')}" />
								
								<g:sortableColumn property="specialist"
								title="${message(code: 'user.specialist.label', default: 'Specialist')}" />

							<g:sortableColumn property="createdate"
								title="${message(code: 'user.createdate.label', default: 'Createdate')}" />

							<g:sortableColumn property="type"
								title="${message(code: 'user.type.label', default: 'Type')}" />

						</tr>
					</thead>
					<tbody>
						<g:each in="${userInstanceList}" status="i" var="userInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

								<td><g:link action="show" id="${userInstance.id}">
										${fieldValue(bean: userInstance, field: "username")}
									</g:link></td>

								<td>
									${fieldValue(bean: userInstance, field: "fullname")}
								</td>
								
								<td>
									${fieldValue(bean: userInstance, field: "specialist")}
								</td>

								<td><g:formatDate date="${userInstance.createdate}" format="dd-MM-yyyy" /></td>

								<td>
									${fieldValue(bean: userInstance, field: "type")}
								</td>

							</tr>
						</g:each>
					</tbody>
				</table>
			</div>
		</div>
		<div class="pagination">
			<g:paginate total="${userInstanceCount ?: 0}" />
		</div>
	</div>
</body>
</html>
