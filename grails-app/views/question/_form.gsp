<%@ page import="com.eximiomedia.Question"%>

<table>
	<tr>
		<td>From:</td>
		<td><div class="picture">
				<g:if test="${questionInstance?.user?.photo}">
					<img class="profile-picture"
						src="http://articles.appdokter.com/${questionInstance?.user?.photo}" />
				</g:if>
				<g:else>
					<g:img class="profile-picture" dir="assets/img" file="profile.png" />
				</g:else>
			</div>
			<div>
				${questionInstance?.user?.nama}
			</div></td>
	</tr>
	<tr>
		<td>Question:</td>
		<td>
			<div
				style="padding: 7px; width: 78% !important; background-color: #fff; border: solid 1px #000; border-radius: 5px;">
				${questionInstance?.question}
			</div>
		</td>
	</tr>
	<tr>
		<td>Answer:</td>
		<td>
			<div
				class="fieldcontain ${hasErrors(bean: questionInstance, field: 'answer', 'error')} ">
				<ckeditor:editor name="answer" height="100px" width="80%">
					${questionInstance?.answer}
				</ckeditor:editor>
			</div>
		</td>
	</tr>
</table>

<g:hiddenField name="status" value="answered" />
