<%@ page import="com.eximiomedia.Question"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'question.label', default: 'Question')}" />
<title><g:message code="default.edit.label" args="[entityName]" /></title>
<style>
.profile-picture {
	margin-left: 5px;
	background-color: #e1e1e1;
	width: 50px;
	height: 50px;
	border-radius: 30px;
	float: left;
}
.even {
	background-color:#fff;
}
.odd {
	background-color:#DBDCDB;
}
</style>
</head>
<body>
	<div class="article-add">
		<div class="nav nav-article">
			<div class="left">
				<span class="icon"></span> <span class="text"
					onclick="window.history.back()">List Questions</span>
			</div>
			<div class="right"></div>
		</div>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<g:hasErrors bean="${questionInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${questionInstance}" var="error">
					<li
						<g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
							error="${error}" /></li>
				</g:eachError>
			</ul>
		</g:hasErrors>
		<g:form url="[resource:questionInstance, action:'update']"
			method="PUT">
			<g:hiddenField name="version" value="${questionInstance?.version}" />
			<fieldset class="form">
				<g:render template="form" />
			</fieldset>
			<fieldset class="buttons">
				<g:actionSubmit class="save" action="update" value="Submit" />
			</fieldset>
		</g:form>
		<g:if test="${otherQuestionList}">
			<div class="lists">
				<table style="width:80% !important">
					<tbody>
						<g:each in="${otherQuestionList}" var="${other}" status="i">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
								<td><g:formatDate date="${other?.questDate}"
										format="dd.MM.yyyy HH:mm:ss" /></td>
								<td><b>Pertanyaan:</b> ${other?.question}</td>
								<td><b>Jawaban:<g:if test="${other?.answer}">
											${other?.answer}
										</g:if> <g:else><g:link class="edit" action="edit"
											resource="${other}">
											<button name="answerbtn" id="answerbtn">Answer Now</button>
										</g:link></g:else></b></td>
							</tr>
						</g:each>
					</tbody>
				</table>
			</div>
		</g:if>
	</div>
</body>
</html>
