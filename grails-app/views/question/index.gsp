
<%@ page import="com.eximiomedia.Question"%>
<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="main">
<g:set var="entityName"
	value="${message(code: 'question.label', default: 'Question')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
<style>
.profile-picture {
	margin-left: 5px;
	background-color: #e1e1e1;
	width: 50px;
	height: 50px;
	border-radius: 30px;
	float: left;
}
</style>
</head>
<body>
	<div class="article-list">
		<div class="nav nav-article">
			<div class="left">
				<span class="text">List Question</span>
			</div>
			<div class="right"></div>
		</div>
		<g:if test="${flash.message}">
			<div class="message" role="status">
				${flash.message}
			</div>
		</g:if>
		<div class="lists">
			<div class="list-item">
				<table>
					<tbody>
						<g:each in="${questionInstanceList}" status="i"
							var="questionInstance">
							<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
								<td width="5%">
									<div class="picture">
										<g:if test="${questionInstance?.user?.photo}">
											<img class="profile-picture"
												src="http://articles.appdokter.com/${questionInstance?.user?.photo}" />
										</g:if>
										<g:else>
											<g:img class="profile-picture" dir="assets/img"
												file="profile.png" />
										</g:else>
									</div>
								</td>

								<td width="15%"><b>Sender:</b> ${questionInstance?.user?.nama}<br> <span
									style="font-size: 12px"><g:formatDate
											date="${questionInstance.questDate}"
											format="dd.MM.yy - HH:mm:ss" /></span>
								</td>

								<td width="70%"><b>Question:</b> ${fieldValue(bean: questionInstance, field: "question")}
								</td>

								<td width="10%"><g:if test="${questionInstance?.status.equals('send')}">
										<g:link class="edit" action="edit"
											resource="${questionInstance}">
											<button name="answerbtn" id="answerbtn">Answer Now</button>
										</g:link>
									</g:if> <g:else>
										${fieldValue(bean: questionInstance, field: "status")}
									</g:else><g:form
										url="[resource:questionInstance, action:'delete']"
										method="DELETE">
										<g:actionSubmit class="delete" action="delete"
											value="${message(code: 'default.button.delete.label', default: 'Delete')}"
											onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
									</g:form></td>

							</tr>
						</g:each>
					</tbody>
				</table>
			</div>
		</div>
		<div class="pagination">
			<g:paginate total="${questionInstanceCount ?: 0}" />
		</div>
	</div>
	
	<script>
		var msgcount = ${questionInstanceCount};
		$("#msgnotif").text(msgcount);
	</script>
</body>
</html>
