package com.eximiomedia

import com.eximiomedia.auth.User

class Question {
	
	User doctor
	AppUser user
	String subject = "-"
	String question
	Date questDate = new Date()
	String answer
	Date ansDate
	String status   // send, answer

    static constraints = {
		answer nullable: true
		ansDate nullable: true
    }
	
	static mapping = {
		subject type:'text'
		question type:'text'
		answer type:'text'
	}
	
	def beforeUpdate() {
		ansDate = new Date()
	}
}
