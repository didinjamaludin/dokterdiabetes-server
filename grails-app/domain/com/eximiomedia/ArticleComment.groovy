package com.eximiomedia

class ArticleComment {
	
	static belongsTo = [article:Article]
	String comment
	AppUser user
	Date commentDate = new Date()
	static hasMany = [replies:CommentReply]

    static constraints = {
    }
	
	static mapping = {
		comment type:'text'
	}
}
