package com.eximiomedia

import java.util.Date;

import com.eximiomedia.auth.User;

class News {
	
	def springSecurityService
	
	String newsTitle
	String newsContent
	Date createDate = new Date()
	User createBy
	Date publishSchedule
	boolean isSchedule

    static constraints = {
		createBy nullable:true
		publishSchedule nullable:true
		isSchedule nullable:true
    }
	
	static mapping = {
		newsContent type:'text'
	}
	
	def beforeInsert() {
		createBy = springSecurityService.currentUser
	}
	
	String toString() {
		newsTitle
	}
}
