package com.eximiomedia.auth

class User {

	transient springSecurityService

	String username
	String password
	String fullname
	String specialist
	String photo
	String hospital
	String type		// fixed, guest
	String notifEmail
	Date createdate = new Date()
	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	static transients = ['springSecurityService']

	static constraints = {
		username blank: false, unique: true
		password blank: false
		fullname blank: false
		specialist blank: false
		photo nullable:true
		type nullable:true
		notifEmail nullable:true
	}

	static mapping = {
		password column: '`password`'
	}

	Set<Authority> getAuthorities() {
		UserAuthority.findAllByUser(this).collect { it.authority } as Set
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService.encodePassword(password)
	}
	
	String toString() {
		fullname
	}
}
