package com.eximiomedia.auth

class Token {
	
	User user
	String token

    static constraints = {
		user unique:true
		token unique:true
    }
}
