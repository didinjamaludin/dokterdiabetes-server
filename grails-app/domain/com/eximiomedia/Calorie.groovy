package com.eximiomedia

class Calorie {
	
	String foodname
	Integer weight
	Integer calorie

    static constraints = {
    }
	
	String toString() {
		foodname+" "+calorie+"/"+weight+"gr"
	}
}
