package com.eximiomedia

import com.eximiomedia.auth.User

class Article {
	
	def springSecurityService
	
	String articleTitle
	String articleAuthor
	String articleContent
	String articleRefence
	String articleImage
	Date createDate = new Date()
	User createBy
	Date publishSchedule
	boolean isSchedule
	static hasMany = [comments:ArticleComment]

    static constraints = {
		articleAuthor nullable:true
		articleRefence nullable:true
		articleImage nullable:true
		publishSchedule nullable:true
		isSchedule nullable:true
		createBy nullable:true
    }
	
	static mapping = {
		articleTitle type:'text'
		articleContent type:'text'
		articleRefence type:'text'
	}
	
	def beforeInsert() {
		createBy = springSecurityService.currentUser
	}
	
	String toString() {
		articleTitle
	}
}
