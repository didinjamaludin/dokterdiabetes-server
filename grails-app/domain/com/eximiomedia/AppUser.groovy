package com.eximiomedia

class AppUser {

	String nama
	String hp
	String kota
	String photo
	String token
	String regid
	boolean eventnot
	boolean chatnot
	boolean replynot
	boolean reminder
	Date createdate = new Date()

	static hasOne = [chatStatus:ChatStatus]

	static constraints = {
		photo nullable: true
		token unique: true
		regid nullable: true
		chatStatus nullable: true
		eventnot nullable: true
		chatnot nullable: true
		replynot nullable: true
		reminder nullable: true
	}
	
	def beforeInsert() {
		eventnot = true
		chatnot = true
		replynot = true
		reminder = true
	}

	String toString() {
		nama
	}
}
