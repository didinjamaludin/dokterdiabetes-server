package com.eximiomedia

class BukaSahur {
	
	String notifType // buka,sahur
	Date notifDate
	String notifMessage

    static constraints = {
		notifType inList:["buka","sahur"]
    }
}
