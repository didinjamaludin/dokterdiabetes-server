package com.eximiomedia

import java.util.Date;

class CommentReply {
	
	static belongsTo = [articleComment:ArticleComment]
	String replyComment
	Date commentDate = new Date()
	AppUser user

    static constraints = {
		user nullable: true
    }
	
	static mapping = {
		comment type:'text'
	}
}
