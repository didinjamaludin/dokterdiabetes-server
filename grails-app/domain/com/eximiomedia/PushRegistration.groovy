package com.eximiomedia

class PushRegistration {
	
	String token
	String regid
	String devtype
	Date createDate

    static constraints = {
		createDate nullable:true
    }
	
	def beforeInsert() {
		createDate = new Date()
	}
}
