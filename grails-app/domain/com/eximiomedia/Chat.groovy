package com.eximiomedia

import com.eximiomedia.auth.User

class Chat {
	
	Date sendDate = new Date()
	User doctor
	AppUser user
	String sender	// doctor, user
	String message
	String image
	String status = "sent" // sent, read, reply

    static constraints = {
		image nullable:true
    }
}
