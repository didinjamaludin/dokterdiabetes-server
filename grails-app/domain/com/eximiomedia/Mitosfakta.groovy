package com.eximiomedia

class Mitosfakta {
	
	String mfTitle
	String mfDesc
	String mfMitfak
	boolean isSchedule
	Date publishSchedule
	Date createDate = new Date()
	String mfImage

    static constraints = {
		mfMitfak inList:["mitos","fakta"]
		publishSchedule nullable:true
		mfImage nullable:true
    }
	
	static mapping = {
		mfTitle type: 'text'
		mfDesc type: 'text'
	}
	
	String toString() {
		mfTitle
	}
}
