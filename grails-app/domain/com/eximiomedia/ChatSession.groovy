package com.eximiomedia

import com.eximiomedia.auth.User

class ChatSession {
	String day
	String startTime
	String endTime
	User doctor
	static hasMany = [chats:Chat]

    static constraints = {
    }
}
