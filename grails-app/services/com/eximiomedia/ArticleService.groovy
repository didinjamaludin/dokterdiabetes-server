package com.eximiomedia

import grails.transaction.Transactional

@Transactional
class ArticleService {

    def androidGcmService

	@Transactional(readOnly = true)
	def articleNotification() {
		def appUser = AppUser.list()

		def articleInstance = Article.withCriteria {
			eq('publishSchedule',new Date().clearTime())
			order('publishSchedule','desc')
		}

		if(articleInstance) {
			Map msg = new HashMap()
			if(articleInstance.articleTitle.getAt(0).size()>40) {
				msg.put("message", articleInstance.articleTitle.getAt(0)[0..40]+"..")
			} else {
				msg.put("message", articleInstance.articleTitle.getAt(0))
			}
			msg.put("title","Artikel kesehatan baru")
			msg.put("msgcnt","1")

			List devices = new ArrayList()

//			appUser.each { u ->
//				devices.add(u.regid)
//			}
//
//			androidGcmService.sendMessage(msg, devices)
			
			appUser.each { u ->
				if(u.regid) {
					devices.add(u.regid)
				}
			}
	
			def part = partition(devices,999)
			
			for(i in 0..part.size()-1) {
				androidGcmService.sendMessage(msg, part[i])
			}
		}
	}
	
	
	def partition(array, size) {
		def partitions = []
		int partitionCount = array.size() / size
	
		partitionCount.times { partitionNumber ->
			def start = partitionNumber * size
			def end = start + size - 1
			partitions << array[start..end]
		}
	
		if (array.size() % size) partitions << array[partitionCount * size..-1]
		return partitions
	}
}
