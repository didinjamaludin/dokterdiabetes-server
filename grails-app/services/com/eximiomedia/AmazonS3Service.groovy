package com.eximiomedia

import grails.transaction.Transactional

import org.jets3t.service.S3Service
import org.jets3t.service.acl.AccessControlList
import org.jets3t.service.impl.rest.httpclient.RestS3Service
import org.jets3t.service.model.S3Bucket
import org.jets3t.service.model.S3Object
import org.jets3t.service.security.AWSCredentials

@Transactional
class AmazonS3Service {

    static String accessKey="AKIAIWVP77V376KO3RWA"
    static String secretKey="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"
    AWSCredentials awsCredentials = new AWSCredentials(accessKey, secretKey);
    boolean transactional = false    
    S3Service s3Service = new RestS3Service(awsCredentials);
    
    def testS3() {
        S3Bucket[] myBuckets = s3Service.listAllBuckets()
        myBuckets.each { b->
            println b.getName()
        }
    }
    
    def putFile(bucket, file) {
        S3Object fileObject = new S3Object(file)
        fileObject.setAcl AccessControlList.REST_CANNED_PUBLIC_READ
        s3Service.putObject(bucket, fileObject)
    }
    
    def delFile(bucket, filename) {
        S3Object fileObject = s3Service.getObject(bucket, filename)
        if(fileObject) {
            s3Service.deleteObject(bucket, fileObject.getKey())
        }
    }
}
