package com.eximiomedia

import grails.transaction.Transactional

@Transactional
class MitosFaktaService {

	def androidGcmService

	@Transactional(readOnly = true)
	def mitosfaktaNotification() {

		def appUser = AppUser.list()

		def mitosfaktaInstance = Mitosfakta.withCriteria {
			eq('publishSchedule',new Date().clearTime())
			order('publishSchedule','desc')
		}

		if(mitosfaktaInstance) {
			Map msg = new HashMap()
			if(mitosfaktaInstance.mfTitle.getAt(0).size()>40) {
				msg.put("message", mitosfaktaInstance.mfTitle.getAt(0)[0..40]+"..")
			} else {
				msg.put("message", mitosfaktaInstance.mfTitle.getAt(0))
			}
			msg.put("title","Mitos dan Fakta baru")
			msg.put("msgcnt","4")

			List devices = new ArrayList()

//			appUser.each { u ->
//				devices.add(u.regid)
//			}
//
//			androidGcmService.sendMessage(msg, devices)
			
			appUser.each { u ->
				if(u.regid) {
					devices.add(u.regid)
				}
			}
	
			def part = partition(devices,999)
			
			for(i in 0..part.size()-1) {
				androidGcmService.sendMessage(msg, part[i])
			}
		}
	}
	
	
	def partition(array, size) {
		def partitions = []
		int partitionCount = array.size() / size
	
		partitionCount.times { partitionNumber ->
			def start = partitionNumber * size
			def end = start + size - 1
			partitions << array[start..end]
		}
	
		if (array.size() % size) partitions << array[partitionCount * size..-1]
		return partitions
	}
}
