package com.eximiomedia

import grails.transaction.Transactional

@Transactional
class BukaSahurService {

	def androidGcmService

	@Transactional(readOnly = true)
	def bukaNotif() {
		def appUser = AppUser.list()

		def buka = BukaSahur.withCriteria {
			and {
				eq('notifDate',new Date().clearTime())
				eq('notifType','buka')
			}
		}

		if(buka) {
			Map msg = new HashMap()
			if(buka.notifMessage.getAt(0).size()>40) {
				msg.put("message", buka.notifMessage.getAt(0)[0..40]+"..")
			} else {
				msg.put("message", buka.notifMessage.getAt(0))
			}
			msg.put("title","Tips Berbuka Puasa")
			msg.put("msgcnt","6")

			List devices = new ArrayList()

			appUser.each { u ->
				if(u.regid) {
					devices.add(u.regid)
				}
			}

			def part = partition(devices,999)

			for(i in 0..part.size()-1) {
				androidGcmService.sendMessage(msg, part[i])
			}
		}
	}
	
	@Transactional(readOnly = true)
	def sahurNotif() {
		def appUser = AppUser.list()

		def sahur = BukaSahur.withCriteria {
			and {
				eq('notifDate',new Date().clearTime())
				eq('notifType','sahur')
			}
		}

		if(sahur) {
			Map msg = new HashMap()
			if(sahur.notifMessage.getAt(0).size()>40) {
				msg.put("message", sahur.notifMessage.getAt(0)[0..40]+"..")
			} else {
				msg.put("message", sahur.notifMessage.getAt(0))
			}
			msg.put("title","Tips Sahur")
			msg.put("msgcnt","6")

			List devices = new ArrayList()

			appUser.each { u ->
				if(u.regid) {
					devices.add(u.regid)
				}
			}

			def part = partition(devices,999)

			for(i in 0..part.size()-1) {
				androidGcmService.sendMessage(msg, part[i])
			}
		}
	}


	def partition(array, size) {
		def partitions = []
		int partitionCount = array.size() / size

		partitionCount.times { partitionNumber ->
			def start = partitionNumber * size
			def end = start + size - 1
			partitions << array[start..end]
		}

		if (array.size() % size) partitions << array[partitionCount * size..-1]
		return partitions
	}
}
