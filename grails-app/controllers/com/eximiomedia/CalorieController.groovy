package com.eximiomedia



import static org.springframework.http.HttpStatus.*
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

import org.springframework.dao.DataIntegrityViolationException
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.grails.plugins.excelimport.*
import java.text.SimpleDateFormat

@Secured(['ROLE_ADMIN'])
@Transactional(readOnly = true)
class CalorieController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
	
	def excelImportService

    def index(Integer max) {
        params.max = Math.min(max ?: 15, 150)
        respond Calorie.list(params), model:[calorieInstanceCount: Calorie.count()]
    }

    def show(Calorie calorieInstance) {
        respond calorieInstance
    }

    def create() {
        respond new Calorie(params)
    }

    @Transactional
    def save(Calorie calorieInstance) {
        if (calorieInstance == null) {
            notFound()
            return
        }

        if (calorieInstance.hasErrors()) {
            respond calorieInstance.errors, view:'create'
            return
        }

        calorieInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'calorieInstance.label', default: 'Calorie'), calorieInstance.id])
                redirect calorieInstance
            }
            '*' { respond calorieInstance, [status: CREATED] }
        }
    }

    def edit(Calorie calorieInstance) {
        respond calorieInstance
    }

    @Transactional
    def update(Calorie calorieInstance) {
        if (calorieInstance == null) {
            notFound()
            return
        }

        if (calorieInstance.hasErrors()) {
            respond calorieInstance.errors, view:'edit'
            return
        }

        calorieInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Calorie.label', default: 'Calorie'), calorieInstance.id])
                redirect calorieInstance
            }
            '*'{ respond calorieInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Calorie calorieInstance) {

        if (calorieInstance == null) {
            notFound()
            return
        }

        calorieInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Calorie.label', default: 'Calorie'), calorieInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
	
	def importcal() {
		
	}
	
	def uploadCal() {
		Map CONFIG_BOOK_COLUMN_MAP = [
			sheet:'Sheet1',
			startRow: 2,
			columnMap:  ['A':'foodname','B':'weight','C':'calorie',]
		]
		def file = request.getFile('calFile')

		Workbook workbook = WorkbookFactory.create(file.inputStream)
		//Iterate through bookList and create/persists your domain instances
		def calList = excelImportService.columns(workbook, CONFIG_BOOK_COLUMN_MAP)
		 
		calList.each { cal ->
			def calorie = Calorie.findByFoodname(cal.foodname)?: new Calorie(foodname:cal.foodname,weight:cal.weight.toInteger(),calorie:cal.calorie.toInteger()).save(flush:true, failOnError:true)
		}
		
		flash.message = "Calorie Imported Successfully"
		render view:"importcal"
	}

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'calorieInstance.label', default: 'Calorie'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
