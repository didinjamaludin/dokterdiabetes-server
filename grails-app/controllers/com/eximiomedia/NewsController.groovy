package com.eximiomedia



import static org.springframework.http.HttpStatus.*
import com.notnoop.apns.APNS
import com.notnoop.apns.ApnsNotification
import com.notnoop.apns.ApnsService
import com.google.android.gcm.server.Message
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
@Transactional(readOnly = true)
class NewsController {

	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	def androidGcmService

	ApnsService apnsService

	def index(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		respond News.list(params), model:[newsInstanceCount: News.count()]
	}

	def show(News newsInstance) {
		respond newsInstance
	}

	def create() {
		respond new News(params)
	}

	@Transactional
	def save(News newsInstance) {
		if (newsInstance == null) {
			notFound()
			return
		}

		if (newsInstance.hasErrors()) {
			respond newsInstance.errors, view:'create'
			return
		}

		newsInstance.save flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.created.message', args: [
					message(code: 'newsInstance.label', default: 'News'),
					newsInstance.id
				])
				redirect newsInstance
			}
			'*' { respond newsInstance, [status: CREATED] }
		}
	}

	def edit(News newsInstance) {
		respond newsInstance
	}

	def sendNotif(News newsInstance) {
		//def appUser = AppUser.list()
		def appUser = AppUser.findAllByNama("Ujang Anom")

		Map msg = new HashMap()
		msg.put("message",newsInstance.id.toString())
		msg.put("title",newsInstance.newsTitle)
		msg.put("msgcnt","6")

		List devices = new ArrayList()

		appUser.each { u ->
			if(u.regid) {
				devices.add(u.regid)
			}
		}

		def part = partition(devices,999)

		for(i in 0..part.size()-1) {
			androidGcmService.sendMessage(msg, part[i])
		}

		/*def regs = PushRegistration.list()

		List regdevs = new ArrayList()

		regs.each { r ->
			if(r.regid) {
				regdevs.add(r.regid)
			}
		}

		def rpart = partition(regdevs,999)

		for(i in 0..rpart.size()-1) {
			androidGcmService.sendMessage(msg, rpart[i])
		}*/
		
		respond newsInstance, view:"show"
	}

	@Transactional
	def update(News newsInstance) {
		if (newsInstance == null) {
			notFound()
			return
		}

		if (newsInstance.hasErrors()) {
			respond newsInstance.errors, view:'edit'
			return
		}

		newsInstance.save flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.updated.message', args: [
					message(code: 'News.label', default: 'News'),
					newsInstance.id
				])
				redirect newsInstance
			}
			'*'{ respond newsInstance, [status: OK] }
		}
	}

	@Transactional
	def delete(News newsInstance) {

		if (newsInstance == null) {
			notFound()
			return
		}

		newsInstance.delete flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.deleted.message', args: [
					message(code: 'News.label', default: 'News'),
					newsInstance.id
				])
				redirect action:"index", method:"GET"
			}
			'*'{ render status: NO_CONTENT }
		}
	}

	protected void notFound() {
		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.not.found.message', args: [
					message(code: 'newsInstance.label', default: 'News'),
					params.id
				])
				redirect action: "index", method: "GET"
			}
			'*'{ render status: NOT_FOUND }
		}
	}
	
	def partition(array, size) {
		def partitions = []
		int partitionCount = array.size() / size

		partitionCount.times { partitionNumber ->
			def start = partitionNumber * size
			def end = start + size - 1
			partitions << array[start..end]
		}

		if (array.size() % size) partitions << array[partitionCount * size..-1]
		return partitions
	}
}
