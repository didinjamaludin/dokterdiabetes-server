package com.eximiomedia



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
@Transactional(readOnly = true)
class ChatScheduleController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond ChatSchedule.list(params), model:[chatScheduleInstanceCount: ChatSchedule.count()]
    }

    def show(ChatSchedule chatScheduleInstance) {
        respond chatScheduleInstance
    }

    def create() {
        respond new ChatSchedule(params)
    }

    @Transactional
    def save(ChatSchedule chatScheduleInstance) {
        if (chatScheduleInstance == null) {
            notFound()
            return
        }

        if (chatScheduleInstance.hasErrors()) {
            respond chatScheduleInstance.errors, view:'create'
            return
        }

        chatScheduleInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'chatScheduleInstance.label', default: 'ChatSchedule'), chatScheduleInstance.id])
                redirect chatScheduleInstance
            }
            '*' { respond chatScheduleInstance, [status: CREATED] }
        }
    }

    def edit(ChatSchedule chatScheduleInstance) {
        respond chatScheduleInstance
    }

    @Transactional
    def update(ChatSchedule chatScheduleInstance) {
        if (chatScheduleInstance == null) {
            notFound()
            return
        }

        if (chatScheduleInstance.hasErrors()) {
            respond chatScheduleInstance.errors, view:'edit'
            return
        }

        chatScheduleInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'ChatSchedule.label', default: 'ChatSchedule'), chatScheduleInstance.id])
                redirect chatScheduleInstance
            }
            '*'{ respond chatScheduleInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(ChatSchedule chatScheduleInstance) {

        if (chatScheduleInstance == null) {
            notFound()
            return
        }

        chatScheduleInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'ChatSchedule.label', default: 'ChatSchedule'), chatScheduleInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'chatScheduleInstance.label', default: 'ChatSchedule'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
