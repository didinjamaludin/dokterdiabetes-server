package com.eximiomedia



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Secured(['ROLE_ADMIN'])
@Transactional(readOnly = true)
class AppVersionController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond AppVersion.list(params), model:[appVersionInstanceCount: AppVersion.count()]
    }

    def show(AppVersion appVersionInstance) {
        respond appVersionInstance
    }

    def create() {
        respond new AppVersion(params)
    }

    @Transactional
    def save(AppVersion appVersionInstance) {
        if (appVersionInstance == null) {
            notFound()
            return
        }

        if (appVersionInstance.hasErrors()) {
            respond appVersionInstance.errors, view:'create'
            return
        }

        appVersionInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'appVersionInstance.label', default: 'AppVersion'), appVersionInstance.id])
                redirect appVersionInstance
            }
            '*' { respond appVersionInstance, [status: CREATED] }
        }
    }

    def edit(AppVersion appVersionInstance) {
        respond appVersionInstance
    }

    @Transactional
    def update(AppVersion appVersionInstance) {
        if (appVersionInstance == null) {
            notFound()
            return
        }

        if (appVersionInstance.hasErrors()) {
            respond appVersionInstance.errors, view:'edit'
            return
        }

        appVersionInstance.save flush:true

        flash.message = "App Version Updated"
		redirect controller: "admin", action:"index"
    }

    @Transactional
    def delete(AppVersion appVersionInstance) {

        if (appVersionInstance == null) {
            notFound()
            return
        }

        appVersionInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'AppVersion.label', default: 'AppVersion'), appVersionInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'appVersionInstance.label', default: 'AppVersion'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
