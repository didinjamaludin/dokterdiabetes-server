package com.eximiomedia



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured
import com.notnoop.apns.APNS
import com.notnoop.apns.ApnsNotification
import com.notnoop.apns.ApnsService

@Secured(['ROLE_DOCTOR','ROLE_ADMIN'])
@Transactional(readOnly = true)
class QuestionController {

	def springSecurityService

	def androidGcmService
	
	ApnsService apnsService

	static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

	def index(Integer max) {
		def query = {
			and {
				eq("doctor",springSecurityService.currentUser)
				eq("status","send")
			}
			order("questDate","desc")
		}
		def criteria = Question.createCriteria()
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def question = criteria.list(query, max: params.max, offset: params.offset)

		respond question, model:[questionInstanceCount: question.getTotalCount()]
	}

	def adminquestion(Integer max) {
		def query = {
			and {
				//eq("doctor",springSecurityService.currentUser)
				eq("status","send")
			}
			order("questDate","desc")
		}
		def criteria = Question.createCriteria()
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def questionInstanceList = criteria.list(query, max: params.max, offset: params.offset)

		respond questionInstanceList, model:[questionInstanceCount: questionInstanceList.getTotalCount()]
	}

	def show(Question questionInstance) {
		respond questionInstance
	}

	def create() {
		respond new Question(params)
	}

	@Transactional
	def save(Question questionInstance) {
		if (questionInstance == null) {
			notFound()
			return
		}

		if (questionInstance.hasErrors()) {
			respond questionInstance.errors, view:'create'
			return
		}

		questionInstance.save flush:true

		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.created.message', args: [
					message(code: 'questionInstance.label', default: 'Question'),
					questionInstance.id
				])
				redirect questionInstance
			}
			'*' { respond questionInstance, [status: CREATED] }
		}
	}

	def edit(Question questionInstance) {
		def otherQuestion = Question.withCriteria {
			and {
				eq("doctor",springSecurityService.currentUser)
				eq("user",Question.get(params.id.toLong()).user)
				ne("id",params.id.toLong())
			}
			order("questDate","desc")
		}

		respond questionInstance,model:[otherQuestionList:otherQuestion]
	}

	@Transactional
	def update(Question questionInstance) {
		if (questionInstance == null) {
			notFound()
			return
		}

		if (questionInstance.hasErrors()) {
			respond questionInstance.errors, view:'edit'
			return
		}

		questionInstance.save flush:true

		if(questionInstance.user.replynot==true) {
			if(questionInstance.user.token.contains("-")) {
				String payload = APNS.newPayload().alertBody("Pertanyaan anda telah di jawab").sound("default").build();
				apnsService.push(questionInstance.user.regid, payload)
			} else {
				Map msg = new HashMap()
				msg.put("message","Pertanyaan anda telah di jawab")
				msg.put("title","Question Notification")
				msg.put("msgcnt","2")

				List devices = new ArrayList()

				devices.add(questionInstance.user.regid)

				androidGcmService.sendMessage(msg, devices);
			}
		}

		request.withFormat {
			form multipartForm {
				flash.message = "Question Answered"
				redirect controller:"question"
			}
			'*'{ respond questionInstance, [status: OK] }
		}
	}

	@Transactional
	def delete(Question questionInstance) {

		if (questionInstance == null) {
			notFound()
			return
		}

		questionInstance.delete flush:true

		if(params.fromList.equals("admin")) {
			redirect action:"adminquestion"
		} else {
			redirect action:"index"
		}
	}

	protected void notFound() {
		request.withFormat {
			form multipartForm {
				flash.message = message(code: 'default.not.found.message', args: [
					message(code: 'questionInstance.label', default: 'Question'),
					params.id
				])
				redirect action: "index", method: "GET"
			}
			'*'{ render status: NOT_FOUND }
		}
	}
}
