package com.eximiomedia

import java.util.concurrent.ScheduledExecutorService;

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional
import com.google.android.gcm.server.Message
import com.eximiomedia.auth.*
import com.notnoop.apns.APNS
import com.notnoop.apns.ApnsNotification
import com.notnoop.apns.ApnsService

@Secured(['ROLE_DOCTOR'])
@Transactional(readOnly = true)
class DoctorController {

	def springSecurityService

	def androidGcmService

	ApnsService apnsService

	def index() {
		def totquest = Question.withCriteria {
			eq("doctor",springSecurityService.currentUser)
		}.size()
		def ansquest = Question.withCriteria {
			and {
				eq("doctor",springSecurityService.currentUser)
				eq("status","answered")
			}
		}.size()
		def notansquest = Question.withCriteria {
			and {
				eq("doctor",springSecurityService.currentUser)
				eq("status","send")
			}
		}.size()

		[totquest:totquest,ansquest:ansquest,notansquest:notansquest]
	}

	def chat() {
	}

	def chatStartNotif() {

		def doctor = User.get(springSecurityService.currentUser.id)
		//def appUser = AppUser.findAllByChatnot(true)
		def appUser = AppUser.list()

		Map msg = new HashMap()
		msg.put("message","Start live chat with "+doctor.fullname)
		//msg.put("message","Livechat kembali lagi tanggal 4 Agustus 2014")
		msg.put("title","Chat Notification")
		msg.put("msgcnt","3")

		List devices = new ArrayList()

		appUser.each { u ->
			if(u.regid) {
				devices.add(u.regid)
			}
		}

		def part = partition(devices,999)

		if(appUser) {
			for(i in 0..part.size()-1) {
				androidGcmService.sendMessage(msg, part[i])
			}
		}

		String payload = APNS.newPayload().alertBody("Start live chat with "+doctor.fullname).sound("default").build();
		def iosUser = AppUser.withCriteria {
			and {
				like("token","%-%")
				//eq("chatnot",true)
			}
		}

		if(iosUser) {
			iosUser.each { u ->
				if(u.regid) {
					apnsService.push(u.regid, payload)
				}
			}
		}

		flash.message = "Chat Start Notification Sent"
		render view:"index"
	}

	def chatStopNotif() {
		def doctor = User.get(springSecurityService.currentUser.id)
		//def appUser = AppUser.findAllByChatnot(true)
		def appUser = AppUser.list()

		Map msg = new HashMap()
		msg.put("message","Stop live chat with "+doctor.fullname)
		msg.put("title","Chat Notification")

		List devices = new ArrayList()

		appUser.each { u ->
			if(u.regid) {
				devices.add(u.regid)
			}
		}

		def part = partition(devices,999)

		if(appUser) {
			for(i in 0..part.size()-1) {
				androidGcmService.sendMessage(msg, part[i])
			}
		}

		String payload = APNS.newPayload().alertBody("Stop live chat with "+doctor.fullname).sound("default").build();
		def iosUser = AppUser.withCriteria {
			and {
				like("token","%-%")
				eq("chatnot",true)
			}
		}

		if(iosUser) {
			iosUser.each { u ->
				if(u.regid) {
					apnsService.push(u.regid, payload)
				}
			}
		}

		flash.message = "Chat Stop Notification Sent"
		render view:"index"
	}

	def question() {
	}

	def chatUserList() {
		def now = new Date()

		def chatUserList = Chat.withCriteria {
			and {
				between("sendDate",now-1,now)
				//eq("sendDate",now.clearTime())
				eq('sender','user')
				eq('status','sent')
			}
			projections {  distinct "user" }
		}

		[chatUserList:chatUserList,chatUserCount:chatUserList.size()]
	}

	def chatMessageList() {
		def now = new Date()

		def chatInstance = Chat.withCriteria {
			and {
				//between("sendDate",now-1,now)
				eq("doctor",springSecurityService.currentUser.id)
				eq("user",params.userid.toLong())
			}
			order('sendDate','asc')
		}

		[chatInstanceList:chatInstance]
	}

	@Transactional
	def answer() {
		def appUser = AppUser.get(params.user.toLong())

		params.user = appUser
		params.doctor = springSecurityService.currentUser

		def chatInstance = new Chat(params)

		chatInstance.save flush:true

		withFormat { json { respond chatInstance } }
	}

	@Transactional
	def chatReply() {

		def otherchat = Chat.withCriteria {
			and {
				eq("status","sent")
				eq("user",AppUser.get(params.user.toLong()))
			}
		}

		otherchat.each { other ->
			other.status = "reply"
			other.save flush:true, failOnerror:true
		}

		def appUser = AppUser.get(params.user.toLong())

		def chatStatus = ChatStatus.findByUser(appUser)
		chatStatus.status = "Chat replied"
		chatStatus.save flush:true,failOnError:true

		params.user = appUser
		params.doctor = springSecurityService.currentUser

		def hari = [
			"Minggu",
			"Senin",
			"Selasa",
			"Rabu",
			"Kamis",
			"Jum'at",
			"Sabtu"
		]

		def date = new Date()

		def schedule = ChatSchedule.withCriteria {
			and {
				eq("dokter",springSecurityService.currentUser)
				eq("day",hari[date.getDay()])
			}
		}

		if(schedule) {
			def etime = schedule.getAt(0).endTime.replace(":","")
			if(etime.toInteger()<date.format("HHmm").toInteger()) {
				def quest = new Question(doctor:springSecurityService.currentUser,user:otherchat.last().user,question:otherchat.last().message,answer:params.message,ansDate:new Date(),status:"answered").save(flush:true,failOnError:true)

				withFormat { json { respond "movechat":"question" } }
			} else {
				def chatInstance = new Chat(params)
				chatInstance.save flush:true
				withFormat { json { respond chatInstance } }
			}
		}


	}

	def partition(array, size) {
		def partitions = []
		int partitionCount = array.size() / size

		partitionCount.times { partitionNumber ->
			def start = partitionNumber * size
			def end = start + size - 1
			partitions << array[start..end]
		}

		if (array.size() % size) partitions << array[partitionCount * size..-1]
		return partitions
	}

	def changeStatus() {
		def appUser = AppUser.get(params.userid.toLong())

		def chatStatus = ChatStatus.findByUser(appUser)
		chatStatus.status = "Doctor read"
		chatStatus.save flush:true,failOnError:true
	}

	def changeStatus2() {
		def appUser = AppUser.get(params.userid.toLong())

		def chatStatus = ChatStatus.findByUser(appUser)
		chatStatus.status = "Doctor typing"
		chatStatus.save flush:true,failOnError:true
	}
}
