package com.eximiomedia


import grails.plugin.springsecurity.annotation.Secured
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Secured(['ROLE_ADMIN'])
@Transactional(readOnly = true)
class BukaSahurController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond BukaSahur.list(params), model:[bukaSahurInstanceCount: BukaSahur.count()]
    }

    def show(BukaSahur bukaSahurInstance) {
        respond bukaSahurInstance
    }

    def create() {
        respond new BukaSahur(params)
    }

    @Transactional
    def save(BukaSahur bukaSahurInstance) {
        if (bukaSahurInstance == null) {
            notFound()
            return
        }

        if (bukaSahurInstance.hasErrors()) {
            respond bukaSahurInstance.errors, view:'create'
            return
        }

        bukaSahurInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'bukaSahurInstance.label', default: 'BukaSahur'), bukaSahurInstance.id])
                redirect bukaSahurInstance
            }
            '*' { respond bukaSahurInstance, [status: CREATED] }
        }
    }

    def edit(BukaSahur bukaSahurInstance) {
        respond bukaSahurInstance
    }

    @Transactional
    def update(BukaSahur bukaSahurInstance) {
        if (bukaSahurInstance == null) {
            notFound()
            return
        }

        if (bukaSahurInstance.hasErrors()) {
            respond bukaSahurInstance.errors, view:'edit'
            return
        }

        bukaSahurInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'BukaSahur.label', default: 'BukaSahur'), bukaSahurInstance.id])
                redirect bukaSahurInstance
            }
            '*'{ respond bukaSahurInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(BukaSahur bukaSahurInstance) {

        if (bukaSahurInstance == null) {
            notFound()
            return
        }

        bukaSahurInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'BukaSahur.label', default: 'BukaSahur'), bukaSahurInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'bukaSahurInstance.label', default: 'BukaSahur'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
