package com.eximiomedia


import grails.plugin.springsecurity.annotation.Secured
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import java.io.File
import java.util.concurrent.ExecutionException
import java.text.SimpleDateFormat

@Secured(['ROLE_ADMIN'])
@Transactional(readOnly = true)
class ArticleController {

    static allowedMethods = [save: "POST", update: "POST", delete: "DELETE"]
	
	def androidGcmService
	
	def amazonS3Service

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Article.listOrderByCreateDate(params), model:[articleInstanceCount: Article.count()]
    }

    def show(Article articleInstance) {
        respond articleInstance
    }

    def create() {		
        respond new Article(params)
    }

    @Transactional
    def save(Article articleInstance) {	
		def file = request.getFile('articleImageFile')
		if(!file.empty) {
			def ftype
			if(file.contentType.equals("image/png"))
			ftype="png"
			if(file.contentType.equals("image/jpg"))
			ftype="jpg"
			if(file.contentType.equals("image/gif"))
			ftype="gif"
			if(file.contentType.equals("image/jpeg"))
			ftype="jpg"
			def webRootDir = servletContext.getRealPath("/")
			def userDir = new File(webRootDir, "/temp")
			def rname = new SimpleDateFormat("ddMMyyHHmmss").format(new Date())
			def fname = "article-"+rname+"."+ftype
			userDir.mkdirs()
			file.transferTo(new File(userDir, fname))
			try {
				def dir = new File(webRootDir, "/temp")
				File tempPicture = new File(dir,fname)
				amazonS3Service.putFile ("articles.appdokter.com",tempPicture)
				tempPicture.delete()
			} catch(InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
			articleInstance.articleImage = fname
		}
		
        if (articleInstance == null) {
            notFound()
            return
        }

        if (articleInstance.hasErrors()) {
            respond articleInstance.errors, view:'create'
            return
        }

        articleInstance.save flush:true
		
//		def appUser = AppUser.list()
//		
//		Map msg = new HashMap()
//		msg.put("message", articleInstance.articleTitle)
//		msg.put("title","New Article")
//		msg.put("msgcnt","1")
//		
//		List devices = new ArrayList()
//		
//		appUser.each { u ->
//			devices.add(u.regid)
//		}
//		
//		androidGcmService.sendMessage(msg, devices);

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'articleInstance.label', default: 'Article'), articleInstance.id])
                redirect articleInstance
            }
            '*' { respond articleInstance, [status: CREATED] }
        }
    }

    def edit(Article articleInstance) {
        respond articleInstance
    }

    @Transactional
    def update(Article articleInstance) {
		def file = request.getFile('articleImageFile')
		if(!file.empty) {
			if(articleInstance.articleImage) {
				amazonS3Service.delFile ("articles.appdokter.com",articleInstance.articleImage)
			}
			def ftype
			if(file.contentType.equals("image/png"))
			ftype="png"
			if(file.contentType.equals("image/jpg"))
			ftype="jpg"
			if(file.contentType.equals("image/gif"))
			ftype="gif"
			if(file.contentType.equals("image/jpeg"))
			ftype="jpg"
			def webRootDir = servletContext.getRealPath("/")
			def userDir = new File(webRootDir, "/temp")
			def rname = new SimpleDateFormat("ddMMyyHHmmss").format(new Date())
			def fname = "article-"+rname+"."+ftype
			userDir.mkdirs()
			file.transferTo(new File(userDir, fname))
			try {
				def dir = new File(webRootDir, "/temp")
				File tempPicture = new File(dir,fname)
				amazonS3Service.putFile ("articles.appdokter.com",tempPicture)
				tempPicture.delete()
			} catch(InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
			articleInstance.articleImage = fname
		}
		
        if (articleInstance == null) {
            notFound()
            return
        }

        if (articleInstance.hasErrors()) {
            respond articleInstance.errors, view:'edit'
            return
        }

        articleInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Article.label', default: 'Article'), articleInstance.id])
                redirect articleInstance
            }
            '*'{ respond articleInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Article articleInstance) {

        if (articleInstance == null) {
            notFound()
            return
        }

        articleInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Article.label', default: 'Article'), articleInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'articleInstance.label', default: 'Article'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
	
	@Transactional
	def replyComment() {
		if(params.comment) {
			def articleComment = ArticleComment.get(params.cid.toLong())
			
			def commentReply = new CommentReply(articleComment:articleComment,replyComment:params.comment).save flush:true,failOnError:true
			
			flash.message = "Comment Replied"
			redirect action:"show",id:params.artid.toLong()
		} else {
			flash.message = "Reply Message can't blank"
			redirect action:"show",id:params.artid.toLong()
		}
	}
	
	@Transactional
	def deleteComment(Long id) {
		def articleComment = ArticleComment.get(id)
		
		articleComment.delete flush:true,failOnError:true
		
		flash.message = "Comment Deleted"
		redirect action:"show",id:params.artid.toLong()
	}
}
