package com.eximiomedia

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

import com.eximiomedia.auth.User

@Secured(['ROLE_ADMIN'])
@Transactional(readOnly = true)
class StatisticController {

	def exportService
	def grailsApplication

	def index() {

		def totalChat = Chat.withCriteria {  eq("sender","user")  }

		[totalUser:AppUser.list().size(),totalProfile: Profile.list().size(),totalQuest:Question.list().size(),totalChat:totalChat.size(),totalpush:PushRegistration.list().size()]
	}

	def chatstat() {

		def chatCount = Chat.withCriteria {
			and {
				if(params.bydoctor) {
					eq('doctor',User.get(params.bydoctor.toLong()))
				}
				if(params.datefrom&&params.dateto) {
					between('sendDate',Date.parse( "dd/MM/yyyy", params.datefrom ),Date.parse( "dd/MM/yyyy", params.dateto))
				}
				if(params.bysender) {
					eq('sender',params.bysender)
				}
				if(params.byuser) {
					ilike('user.nama',params.byuser)
				}
			}

			projections { distinct("user") }
		}

		def query = {
			and {
				if(params.bydoctor) {
					eq('doctor',User.get(params.bydoctor.toLong()))
				}
				if(params.datefrom&&params.dateto) {
					between('sendDate',Date.parse( "dd/MM/yyyy", params.datefrom ),Date.parse( "dd/MM/yyyy", params.dateto))
				}
				if(params.bysender) {
					eq('sender',params.bysender)
				}
				if(params.byuser) {
					ilike('user.nama',params.byuser)
				}
			}
		}
		def criteria = Chat.createCriteria()
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def chatInstanceList = criteria.list(query, max: params.max, offset: params.offset)
		def filters = [bydoctor:params.bydoctor,datefrom:params.datefrom,dateto:params.dateto,bysender:params.bysender,byuser:params.byuser]
		def model = [chatCount: chatCount.size(), chatInstanceList: chatInstanceList, chatInstanceTotal: chatInstanceList.totalCount, filters:filters]

		if(params?.formatfile && params.formatfile != "html") {
			response.contentType = grailsApplication.config.grails.mime.types[params.format]
			response.setHeader("Content-disposition", "attachment; filename=books.${params.extension}")

			exportService.export(params.formatfile, response.outputStream,criteria.list(query), [:], [:])
		}

		if(request.xhr) {
			render(template: "chatstatgrid", model: model)
		} else {
			model
		}
	}

	def queststat() {

		def questCount = Question.withCriteria {
			and {
				if(params.bydoctor) {
					println params.bydoctor
					eq('doctor',User.get(params.bydoctor.toLong()))
				}
				if(params.datefrom&&params.dateto) {
					between('questDate',Date.parse( "dd/MM/yyyy", params.datefrom ),Date.parse( "dd/MM/yyyy", params.dateto))
				}
				if(params.byuser) {
					ilike('user.nama',params.byuser)
				}
				if(params.byquestion) {
					ilike('question',params.byquestion)
				}
			}

			projections { distinct("user") }
		}

		def query = {
			and {
				if(params.bydoctor) {
					println params.bydoctor
					eq('doctor',User.get(params.bydoctor.toLong()))
				}
				if(params.datefrom&&params.dateto) {
					between('questDate',Date.parse( "dd/MM/yyyy", params.datefrom ),Date.parse( "dd/MM/yyyy", params.dateto))
				}
				if(params.byuser) {
					ilike('user.nama',params.byuser)
				}
				if(params.byquestion) {
					ilike('question',params.byquestion)
				}
			}
		}
		def criteria = Question.createCriteria()
		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def questInstanceList = criteria.list(query, max: params.max, offset: params.offset)
		def filters = [bydoctor:params.bydoctor,datefrom:params.datefrom,dateto:params.dateto,byuser:params.byuser,byquestion:params.byquestion]
		def model = [questCount: questCount.size(), questInstanceList: questInstanceList, questInstanceTotal: questInstanceList.totalCount, filters:filters]

		if(params?.formatfile && params.formatfile != "html") {
			response.contentType = grailsApplication.config.grails.mime.types[params.format]
			response.setHeader("Content-disposition", "attachment; filename=books.${params.extension}")

			exportService.export(params.formatfile, response.outputStream,criteria.list(query), [:], [:])
		}

		if(request.xhr) {
			render(template: "queststatgrid", model: model)
		} else {
			model
		}
	}

	def profilelist() {

		def profile = Profile.list()

		def profileuser = []

		profile.each { p ->
			def user = AppUser.findByToken(p.token)

			def prof = [:]
			if(user) {
				prof.put("nama",user.nama)
				prof.put("hp",user.hp)
				prof.put("kota",user.kota)
				prof.put("gender",p.gender)
				prof.put("aktivitas",p.aktivitas)
				prof.put("berat",p.berat)
				prof.put("tinggi",p.tinggi)

				profileuser.add(prof)
			}
		}

		if(params?.formatfile && params.formatfile != "html") {
			response.contentType = grailsApplication.config.grails.mime.types[params.format]
			response.setHeader("Content-disposition", "attachment; filename=books.${params.extension}")

			exportService.export(params.formatfile, response.outputStream,profileuser, [:], [:])
		}
	}

	def refreshpush() {
		def tokens = AppUser.withCriteria {
			projections {   property('token') }
		}

		tokens.each { t ->
			def pushreg = PushRegistration.findByToken(t)
			if(pushreg) {
				pushreg.delete()
			}
		}

		redirect action:"index"
	}
}
