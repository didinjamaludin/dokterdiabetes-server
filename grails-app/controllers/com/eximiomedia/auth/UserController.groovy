package com.eximiomedia.auth



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured
import java.text.SimpleDateFormat
import java.io.File
import java.util.concurrent.ExecutionException

@Secured(['ROLE_ADMIN'])
@Transactional(readOnly = true)
class UserController {

    static allowedMethods = [save: "POST", update: "POST", delete: "DELETE"]
	
	def amazonS3Service

    def index(Integer max) {
		def userInstance = UserAuthority.findAllByAuthority(Authority.findByAuthority("ROLE_DOCTOR")).user
        
		respond userInstance
    }

    def show(User userInstance) {
        respond userInstance
    }

    def create() {
        respond new User(params)
    }

    @Transactional
    def save(User userInstance) {
		def file = request.getFile('photoFile')
		if(!file.empty) {
			def ftype
			if(file.contentType.equals("image/png"))
			ftype="png"
			if(file.contentType.equals("image/jpg"))
			ftype="jpg"
			if(file.contentType.equals("image/gif"))
			ftype="gif"
			if(file.contentType.equals("image/jpeg"))
			ftype="jpg"
			def webRootDir = servletContext.getRealPath("/")
			def userDir = new File(webRootDir, "/temp")
			def rname = new SimpleDateFormat("ddMMyyHHmmss").format(new Date())
			def fname = "doctor-"+rname+"."+ftype
			userDir.mkdirs()
			file.transferTo(new File(userDir, fname))
			try {
				def dir = new File(webRootDir, "/temp")
				File tempPicture = new File(dir,fname)
				amazonS3Service.putFile ("articles.appdokter.com",tempPicture)
				tempPicture.delete()
			} catch(InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
			userInstance.photo = fname
		}
		
        if (userInstance == null) {
            notFound()
            return
        }

        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:'create'
            return
        }

        userInstance.save flush:true
		
		UserAuthority.create(userInstance,Authority.findByAuthority("ROLE_DOCTOR")) 

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'userInstance.label', default: 'User'), userInstance.id])
                redirect userInstance
            }
            '*' { respond userInstance, [status: CREATED] }
        }
    }

    def edit(User userInstance) {
        respond userInstance
    }

    @Transactional
    def update(User userInstance) {
		def file = request.getFile('photoFile')
		if(!file.empty) {
			if(userInstance.photo) {
				amazonS3Service.delFile ("articles.appdokter.com",userInstance.photo)
			}
			def ftype
			if(file.contentType.equals("image/png"))
			ftype="png"
			if(file.contentType.equals("image/jpg"))
			ftype="jpg"
			if(file.contentType.equals("image/gif"))
			ftype="gif"
			if(file.contentType.equals("image/jpeg"))
			ftype="jpg"
			def webRootDir = servletContext.getRealPath("/")
			def userDir = new File(webRootDir, "/temp")
			def rname = new SimpleDateFormat("ddMMyyHHmmss").format(new Date())
			def fname = "doctor-"+rname+"."+ftype
			userDir.mkdirs()
			file.transferTo(new File(userDir, fname))
			try {
				def dir = new File(webRootDir, "/temp")
				File tempPicture = new File(dir,fname)
				amazonS3Service.putFile ("articles.appdokter.com",tempPicture)
				tempPicture.delete()
			} catch(InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
			userInstance.photo = fname
		}
		
        if (userInstance == null) {
            notFound()
            return
        }

        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:'edit'
            return
        }

        userInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'User.label', default: 'User'), userInstance.id])
                redirect userInstance
            }
            '*'{ respond userInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(User userInstance) {

        if (userInstance == null) {
            notFound()
            return
        }

        userInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'User.label', default: 'User'), userInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'userInstance.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
