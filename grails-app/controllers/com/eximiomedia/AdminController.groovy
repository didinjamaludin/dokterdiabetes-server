package com.eximiomedia

import com.google.android.gcm.server.Message

import grails.converters.JSON
import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

import com.notnoop.apns.APNS
import com.notnoop.apns.ApnsNotification
import com.notnoop.apns.ApnsService

@Secured(['ROLE_ADMIN'])
@Transactional(readOnly = true)
class AdminController {

	def chatScheduleService

	def androidGcmService

	ApnsService apnsService

	def index = {
		def appVersionInstance = AppVersion.findAll() 
		
		[appVersionInstance:appVersionInstance]
	}

	def sendMessage() {
		//chatScheduleService.checkLiveChat()
		if(params.sendAll.equals("all")) {
			def appUser = AppUser.list()

			Map msg = new HashMap()
			msg.put("message",params.msgText)
			msg.put("title",params.msgTitle)
			msg.put("msgcnt","5")

			List devices = new ArrayList()

			appUser.each { u ->
				if(u.regid) {
					devices.add(u.regid)
				}
			}

			def part = partition(devices,999)

			for(i in 0..part.size()-1) {
				androidGcmService.sendMessage(msg, part[i])
			}

			def regs = PushRegistration.list()

			List regdevs = new ArrayList()

			regs.each { r ->
				if(r.regid) {
					regdevs.add(r.regid)
				}
			}

			def rpart = partition(regdevs,999)

			for(i in 0..rpart.size()-1) {
				androidGcmService.sendMessage(msg, rpart[i])
			}
		} else if(params.sendAll.equals("member")) {
			def appUser = AppUser.list()

			Map msg = new HashMap()
			msg.put("message",params.msgText)
			msg.put("title",params.msgTitle)
			msg.put("msgcnt","5")

			List devices = new ArrayList()

			appUser.each { u ->
				if(u.regid) {
					devices.add(u.regid)
				}
			}

			def part = partition(devices,999)

			for(i in 0..part.size()-1) {
				androidGcmService.sendMessage(msg, part[i])
			}

		} else if(params.sendAll.equals("nonmember")) {
			Map msg = new HashMap()
			msg.put("message",params.msgText)
			msg.put("title",params.msgTitle)
			msg.put("msgcnt","5")

			def regs = PushRegistration.list()

			List regdevs = new ArrayList()

			regs.each { r ->
				if(r.regid) {
					regdevs.add(r.regid)
				}
			}

			def rpart = partition(regdevs,999)

			for(i in 0..rpart.size()-1) {
				androidGcmService.sendMessage(msg, rpart[i])
			}

		} else {
			List devices = new ArrayList()

			Map msg = new HashMap()
			msg.put("message",params.msgText)
			msg.put("title",params.msgTitle)
			msg.put("msgcnt","5")

			for(i in 0..(params.rows.toInteger()-1)) {
				if(AppUser.get(params["user["+i+"]"].toLong()).regid) {
					devices.add(AppUser.get(params["user["+i+"]"].toLong()).regid)
				}
			}

			androidGcmService.sendMessage(msg, devices)
		}

		flash.message = "Message sent"
		redirect action:"index"
	}

	def sendMessageToDevices() {
		String payload = APNS.newPayload().alertBody(params.msgTitle2).sound("default").build();
		
		if(params.sendAll2.equals("all2")) {
			def appUser = AppUser.withCriteria {
				like("token","%-%")
			}

			appUser.each { u ->
				if(u.regid) {
					apnsService.push(u.regid, payload)
				}
			}
			
			def pushReg = PushRegistration.withCriteria {
				like("token","%-%")
			}
			
			pushReg.each { p ->
				if(p.regid) {
					apnsService.push(p.regid, payload)
				}
			}
		} 
		else if(params.sendAll2.equals("member2")) {
			def appUser = AppUser.withCriteria {
				like("token","%-%")
			}

			appUser.each { u ->
				if(u.regid) {
					apnsService.push(u.regid, payload)
				}
			}

		} else if(params.sendAll2.equals("nonmember2")) {
			def pushReg = PushRegistration.withCriteria {
				like("token","%-%")
			}
			
			pushReg.each { u ->
				if(u.regid) {
					apnsService.push(u.regid, payload)
				}
			}

		} else {
			for(i in 0..(params.rows2.toInteger()-1)) {
				def regid = AppUser.get(params["user2["+i+"]"].toLong()).regid
				if(regid) {
					apnsService.push(regid, payload)
				}
			}
		}

		flash.message = "Message sent"
		redirect action:"index"
	}

	def partition(array, size) {
		def partitions = []
		int partitionCount = array.size() / size

		partitionCount.times { partitionNumber ->
			def start = partitionNumber * size
			def end = start + size - 1
			partitions << array[start..end]
		}

		if (array.size() % size) partitions << array[partitionCount * size..-1]
		return partitions
	}

	def getUser() {
		def userFound = AppUser.withCriteria {
			ilike 'nama', params.term + '%'
		}

		def userList = []

		userFound.each { u ->
			def uMap = [:]
			uMap.put("id", u.id)
			uMap.put("label", u.nama)
			uMap.put("value", u.nama)
			userList.add(uMap)
		}

		render (userList as JSON)
	}
	
	@Transactional
	def saveAppVersion(AppVersion appVersionInstance) {
		if (appVersionInstance == null) {
			notFound()
			return
		}

		if (appVersionInstance.hasErrors()) {
			respond appVersionInstance.errors, view:'create'
			return
		}

		appVersionInstance.save flush:true

		flash.message = "App Version Created"
		redirect action:"index"
	}
	
	@Transactional
	def updateAppVersion(AppVersion appVersionInstance) {
		if (appVersionInstance == null) {
			notFound()
			return
		}

		if (appVersionInstance.hasErrors()) {
			respond appVersionInstance.errors, view:'edit'
			return
		}

		appVersionInstance.save flush:true

		flash.message = "App Version Updated"
		redirect action:"index"
	}
	
}
