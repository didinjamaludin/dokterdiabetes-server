package com.eximiomedia

import grails.converters.JSON
import grails.transaction.Transactional
import groovy.json.JsonSlurper
import com.eximiomedia.auth.*
import java.text.SimpleDateFormat
import java.util.concurrent.ExecutionException
import java.io.File
import java.awt.image.BufferedImage
import java.awt.Graphics2D
import javax.imageio.ImageIO
import org.apache.commons.io.FileUtils
import com.mortennobel.imagescaling.*

class ApiController {

	static allowedMethods = [getNewItem: "GET", register: "POST", chatSend: "POST", sendFile: "POST", getDoctor: "GET", receiveChat: "GET", sendComment:"POST",getArticles:"GET",getComments:"GET",showArticle:"GET",getMitosFakta:"GET", getCalorie:"GET",getChatSchedule:"GET",getAnswer:"GET",sendQuestion:"POST",getMessage:"GET",updateprofile:"POST",sendRegId:"POST",checkServerUser:"GET",getArticleByTitle:"GET",sendProfile:"POST"]

	static responseFormats = ['json']

	def amazonS3Service

	def mailService

	def getNewItem() {

		def appVersion = AppVersion.findAll()

		def lastMF = Mitosfakta.findByPublishSchedule(new Date().clearTime())

		def lastArticle = Article.findByPublishSchedule(new Date().clearTime())

		def lastNews = News.findByPublishSchedule(new Date().clearTime())

		def results = [appVersion:appVersion,lastMF:lastMF,lastArticle:lastArticle,lastNews:lastNews]

		withFormat { json { respond results
			} }
	}

	@Transactional
	def register() {

		def file = request.getFile('photofile')
		if(file) {
			def ftype
			if(file.contentType.equals("image/png"))
				ftype="png"
			if(file.contentType.equals("image/jpg"))
				ftype="jpg"
			if(file.contentType.equals("image/gif"))
				ftype="gif"
			if(file.contentType.equals("image/jpeg"))
				ftype="jpg"
			if(file.contentType.equals("application/octet-stream"))
				ftype="jpg"
			def webRootDir = servletContext.getRealPath("/")
			def userDir = new File(webRootDir, "/temp")
			def rname = new SimpleDateFormat("ddMMyyHHmmss").format(new Date())
			def fname = "photo-"+rname+"."+ftype
			userDir.mkdirs()
			file.transferTo(new File(userDir, fname))
			try {
				def dir = new File(webRootDir, "/temp")
				File tempPicture = new File(dir,fname)
				BufferedImage image = ImageIO.read(tempPicture)
				ResampleOp resampleOp = new ResampleOp(81,81)
				resampleOp.setFilter(ResampleFilters.getLanczos3Filter())
				resampleOp.setUnsharpenMask(AdvancedResizeOp.UnsharpenMask.Normal)
				BufferedImage scaledImage = resampleOp.filter(image,null)
				File scalePicture = new File(dir,"scphoto-"+rname+"."+ftype)
				ImageIO.write(scaledImage,"jpg",scalePicture)
				amazonS3Service.putFile ("articles.appdokter.com",scalePicture)
				tempPicture.delete()
				scalePicture.delete()
			} catch(InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
			params.photo = "sc"+fname
		}

		def appUser = AppUser.findByToken(params.token)

		if(appUser) {
			withFormat { json { respond  appUser
				} }
		} else {
			def userInstance = new AppUser(params)

			userInstance.save flush:true,failOnError:true

			withFormat { json { respond  userInstance
				} }
		}
	}

	@Transactional
	def updateprofile() {
		if(!params.token) {
			withFormat {
				json  {
					response.status = 403
					render "Forbidden Access"
				}
			}
		} else {
			def file = request.getFile('photofile')
			if(file) {
				if(params.photo) {
					amazonS3Service.delFile ("articles.appdokter.com",params.photo)
				}
				def ftype
				if(file.contentType.equals("image/png"))
					ftype="png"
				if(file.contentType.equals("image/jpg"))
					ftype="jpg"
				if(file.contentType.equals("image/gif"))
					ftype="gif"
				if(file.contentType.equals("image/jpeg"))
					ftype="jpg"
				if(file.contentType.equals("application/octet-stream"))
					ftype="jpg"
				def webRootDir = servletContext.getRealPath("/")
				def userDir = new File(webRootDir, "/temp")
				def rname = new SimpleDateFormat("ddMMyyHHmmss").format(new Date())
				def fname = "photo-"+rname+"."+ftype
				userDir.mkdirs()
				file.transferTo(new File(userDir, fname))
				try {
					def dir = new File(webRootDir, "/temp")
					File tempPicture = new File(dir,fname)
					BufferedImage image = ImageIO.read(tempPicture)
					ResampleOp resampleOp = new ResampleOp(81,81)
					resampleOp.setFilter(ResampleFilters.getLanczos3Filter())
					resampleOp.setUnsharpenMask(AdvancedResizeOp.UnsharpenMask.Normal)
					BufferedImage scaledImage = resampleOp.filter(image,null)
					File scalePicture = new File(dir,"scphoto-"+rname+"."+ftype)
					ImageIO.write(scaledImage,"jpg",scalePicture)
					amazonS3Service.putFile ("articles.appdokter.com",scalePicture)
					tempPicture.delete()
					scalePicture.delete()
				} catch(InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
				params.photo = "sc"+fname
			}

			def userInstance = AppUser.findByToken(params.token)

			if(!userInstance) {
				withFormat {
					json  {
						response.status = 404
						render "Wrong key"
					}
				}
			} else {
				if(params.eventnot) {
					params.eventnot = true
				} else {
					params.eventnot = false
				}
				
				if(params.chatnot) {
					params.chatnot = true
				} else {
					params.chatnot = false
				}
				
				if(params.replynot) {
					params.replynot = true
				} else {
					params.replynot = false
				}
				
				if(params.reminder) {
					params.reminder = true
				} else {
					params.reminder = false
				}
			
				userInstance.properties = params

				userInstance.save flush:true, failOnError:true

				withFormat { json { respond  userInstance
					} }
			}
		}
	}

	@Transactional
	def chatSend() {
		if(!params.token) {
			withFormat {
				json  {
					response.status = 403
					render "Forbidden Access"
				}
			}
		} else {
			def appUser = AppUser.findByToken(params.token.trim())

			if(!appUser) {
				withFormat {
					json  {
						response.status = 404
						render "Wrong key"
					}
				}
			} else {
				def doc
				if(params.gizidoctor) {
					params.doctor = User.get(params.gizidoctor.toLong())
				}
				if(params.jandoctor) {
					params.doctor = User.get(params.jandoctor.toLong())
				}
				if(params.pddoctor) {
					params.doctor = User.get(params.pddoctor.toLong())
				}
				if(params.guestdoctor) {
					params.doctor = User.get(params.guestdoctor.toLong())
				}
				params.user = appUser

				def chatInstance = new Chat(params)

				chatInstance.save flush:true, failOnError:true

				def chatStatus = ChatStatus.findByUser(appUser)?:new ChatStatus(user:appUser,status:"sent").save(flush:true,failOnError:true)
				chatStatus.status = "User sent"
				chatStatus.save flush:true, failOnError:true

				//				mailService.sendMail {
				//					to "fred@g2one.com","ginger@g2one.com"
				//					from "Dokter Diabetes<pocarigoion@gmail.com>"
				//					subject "Hello John"
				//					body 'this is some text'
				//				 }

				withFormat { json { respond chatInstance } }
			}
		}
	}

	def receiveChat() {
		if(!params.token) {
			withFormat {
				json  {
					response.status = 403
					render "Forbidden Access"
				}
			}
		} else {
			def appUser = AppUser.findByToken(params.token.trim())

			if(!appUser) {
				withFormat {
					json  {
						response.status = 404
						render "Wrong key"
					}
				}
			} else {
				def now = new Date()
				params.doctor = User.get(params.doctor.toLong())
				def chatInstance = Chat.withCriteria {
					and {
						eq('doctor',params.doctor)
						eq('user',appUser)
						between('sendDate',now-1,now)
					}
					maxResults(20)
					order("sendDate", "asc")
				}

				def chatList = []

				chatInstance.each { c ->
					def cmap = [:]
					def doctor = User.get(c.doctor.id)
					cmap.put("docphoto",doctor.photo)
					cmap.put("usrphoto",appUser.photo)
					cmap.put("sender",c.sender)
					cmap.put("message",c.message)
					cmap.put("image",c.image)
					cmap.put("sendDate",c.sendDate)
					chatList.add(cmap)
				}

				withFormat { json { respond chatList } }
			}
		}
	}

	def getDoctor() {
		def users = UserAuthority.findAllByAuthority(Authority.findByAuthority("ROLE_DOCTOR")).user

		def userInstance = []

		users.each { u ->
			userInstance.add(id:u.id,fullname:u.fullname,specialist:u.specialist,photo:u.photo,hospital:u.hospital,type:u.type)
		}

		withFormat {
			json { render userInstance as JSON }
		}
	}

	def getArticles() {

		if(!params.token) {
			withFormat {
				json  {
					response.status = 403
					render "Forbidden Access"
				}
			}
		} else {
			def appUser = AppUser.findByToken(params.token.trim())

			if(!appUser) {
				withFormat {
					json  {
						response.status = 404
						render "Wrong key"
					}
				}
			} else {
				def articles = Article.withCriteria {
					and {
						or {
							eq("isSchedule",false)
							eq("publishSchedule",new Date().clearTime())
						}
						if(params.lastdate) {
							gt('createDate',new Date(params.lastdate))
						}
					}
					order('createDate','desc')
					maxResults(10)
				}

				withFormat {
					json { render articles as JSON }
				}
			}
		}
	}

	def getArticleByTitle() {
		if(!params.token) {
			withFormat {
				json  {
					response.status = 403
					render "Forbidden Access"
				}
			}
		} else {
			def appUser = AppUser.findByToken(params.token.trim())

			if(!appUser) {
				withFormat {
					json  {
						response.status = 404
						render "Wrong key"
					}
				}
			} else {
				def articles = Article.withCriteria {
					ilike('articleTitle','%'+params.terms+'%')

					order('createDate','desc')
					maxResults(10)
				}

				withFormat {
					json { render articles as JSON }
				}
			}
		}
	}

	@Transactional
	def sendComment() {
		if(!params.token) {
			withFormat {
				json  {
					response.status = 403
					render "Forbidden Access"
				}
			}
		} else {
			def appUser = AppUser.findByToken(params.token.trim())

			if(!appUser) {
				withFormat {
					json  {
						response.status = 404
						render "Wrong key"
					}
				}
			} else {
				params.user = appUser

				def comment = new ArticleComment(params)

				comment.save flush:true

				withFormat { json { respond comment } }
			}
		}
	}

	def getComments() {
		if(!params.token) {
			withFormat {
				json  {
					response.status = 403
					render "Forbidden Access"
				}
			}
		} else {
			def appUser = AppUser.findByToken(params.token.trim())

			if(!appUser) {
				withFormat {
					json  {
						response.status = 404
						render "Wrong key"
					}
				}
			} else {
				def comments = ArticleComment.withCriteria {
					and {
						eq("article",Article.get(params.id.toLong()))
						if(params.lastid) {
							gt("id",params.lastid.toLong())
						}
					}
					order("commentDate","desc")
					maxResults(10)
				}

				def comlist = []

				comments.each { c ->
					def cmap = [:]
					cmap.put("article",c.article)
					cmap.put("comment",c.comment)
					cmap.put("commentDate",c.commentDate)
					cmap.put("nama",c.user.nama)
					cmap.put("photo",c.user.photo)
					cmap.put("replyComment", c.replies.replyComment)
					comlist.add(cmap)
				}

				withFormat { json { respond comlist } }
			}
		}
	}

	def showArticle() {
		if(!params.token) {
			withFormat {
				json  {
					response.status = 403
					render "Forbidden Access"
				}
			}
		} else {
			def appUser = AppUser.findByToken(params.token.trim())

			if(!appUser) {
				withFormat {
					json  {
						response.status = 404
						render "Wrong key"
					}
				}
			} else {
				def article = Article.get(params.id.toLong())

				withFormat {
					json { render article as JSON }
				}
			}
		}
	}

	def getMitosFakta() {
		def mitosfakta = Mitosfakta.withCriteria {
			or {
				eq('isSchedule',false)
				eq('publishSchedule',new Date().clearTime())
			}
		}

		withFormat {
			json { render mitosfakta as JSON }
		}
	}

	def getCalorie() {
		def calorie = Calorie.withCriteria {
			gt('id',params.lastcal.toLong())
		}

		withFormat {
			json { render calorie as JSON }
		}
	}

	def getChatSchedule() {
		def schedule = ChatSchedule.list()

		withFormat {
			json { render schedule as JSON }
		}
	}

	@Transactional
	def sendQuestion() {
		if(!params.token) {
			withFormat {
				json  {
					response.status = 403
					render "Forbidden Access"
				}
			}
		} else {
			def appUser = AppUser.findByToken(params.token.trim())

			if(!appUser) {
				withFormat {
					json  {
						response.status = 404
						render "Wrong key"
					}
				}
			} else {
				params.user = appUser
				params.doctor = User.get(params.docid.toLong())
				//params.answer = "Selamat Hari Raya Idul Fitri 1435 H, Mohon Maaf Lahir dan Batin. Terima kasih atas kepercayaan Anda terhadap kami. Layanan Konsultasi Dokter akan kembali lagi pada tanggal 4 Agustus 2014."
				params.answer = "Pertanyaan anda akan di jawab paling lambat 1x2 jam"
				params.ansDate = new Date()
				params.status = "send"

				def question = new Question(params)

				question.save flush:true, failOnError:true

				def receiver

				params.doctor.notifEmail.splitEachLine(",") { receiver = it }
				
				receiver.each { r ->

					mailService.sendMail {
						to r
						from "Dokter Diabetes<admin@dokterdiabetes.com>"
						subject "Notifikasi Pertanyaan Offline dari App DokterDiabetes"
						html '<p>Dear '+params.doctor.fullname+',<br><br><h3>Dari  '+params.user.nama+':</h3><br>'+params.question+'<br><br>Regards,<br>Dokter Diabetes</p>'
					}
				}

				withFormat { json { respond question } }
			}
		}
	}

	def getAnswer() {
		if(!params.token) {
			withFormat {
				json  {
					response.status = 403
					render "Forbidden Access"
				}
			}
		} else {
			def appUser = AppUser.findByToken(params.token.trim())

			if(!appUser) {
				withFormat {
					json  {
						response.status = 404
						render "Wrong key"
					}
				}
			} else {
				def answer = Question.findAllByUser(appUser, [max: 10, sort: "questDate", order: "desc"])

				withFormat {
					json { render answer as JSON }
				}
			}
		}
	}

	def getMessage() {
		if(!params.token) {
			withFormat {
				json  {
					response.status = 403
					render "Forbidden Access"
				}
			}
		} else {
			def appUser = AppUser.findByToken(params.token.trim())

			if(!appUser) {
				withFormat {
					json  {
						response.status = 404
						render "Wrong key"
					}
				}
			} else {
				def message = Question.withCriteria {
					and {
						eq('user',appUser)
						eq('doctor',User.get(params.docid.toLong()))
						if(params.lastdate) {
							gt('questDate',new Date(params.lastdate))
						}
					}
					order('questDate','desc')
					maxResults(10)
				}

				withFormat {
					json { render message as JSON }
				}
			}
		}
	}

	@Transactional
	def sendRegId() {
		if(!params.token) {
			withFormat {
				json  {
					response.status = 403
					render "Forbidden Access"
				}
			}
		} else {
			def appUser = AppUser.findByToken(params.token.trim())

			if(!appUser) {
				if(params.token.contains("-")) {
					params.devtype = "ios"
				} else {
					params.devtype = "android"
				}

				def pushreg = PushRegistration.findByToken(params.token)

				if(!pushreg) {
					new PushRegistration(params).save flush:true, failOnError:true
				}

				withFormat {
					json  {
						response.status = 200
						render "save to Push Registration"
					}
				}
			} else {
				appUser.properties = params

				appUser.save flush:true, failOnError:true

				withFormat { json { respond appUser } }
			}
		}
	}

	@Transactional
	def sendFile() {
		if(!params.token) {
			withFormat {
				json  {
					response.status = 403
					render "Forbidden Access"
				}
			}
		} else {
			def appUser = AppUser.findByToken(params.token.trim())
			if(!appUser) {
				withFormat {
					json  {
						response.status = 404
						render "Wrong key"
					}
				}
			} else {
				def file = request.getFile('filename')
				if(file) {
					def ftype
					if(file.contentType.equals("image/png"))
						ftype="png"
					if(file.contentType.equals("image/jpg"))
						ftype="jpg"
					if(file.contentType.equals("image/gif"))
						ftype="gif"
					if(file.contentType.equals("image/jpeg"))
						ftype="jpg"
					if(file.contentType.equals("application/octet-stream"))
						ftype="jpg"
					def webRootDir = servletContext.getRealPath("/")
					def userDir = new File(webRootDir, "/temp")
					def rname = new SimpleDateFormat("ddMMyyHHmmss").format(new Date())
					def fname = "chat-"+rname+"."+ftype
					userDir.mkdirs()
					file.transferTo(new File(userDir, fname))
					try {
						def dir = new File(webRootDir, "/temp")
						File tempPicture = new File(dir,fname)
						amazonS3Service.putFile ("articles.appdokter.com",tempPicture)
						tempPicture.delete()
					} catch(InterruptedException e) {
						e.printStackTrace();
					} catch (ExecutionException e) {
						e.printStackTrace();
					}
					params.message = "http://articles.appdokter.com/"+fname
				}

				params.doctor = User.get(params.docid.toLong())
				params.user = appUser
				params.sender = 'user'
				def chatInstance = new Chat(params)
				chatInstance.save flush:true, failOnError:true
				withFormat { json { respond chatInstance } }
			}
		}
	}

	def checkServerUser() {
		if(!params.token) {
			withFormat {
				json  {
					response.status = 403
					render "Forbidden Access"
				}
			}
		} else {
			def appUser = AppUser.findByToken(params.token.trim())

			if(appUser) {
				withFormat {
					json { render appUser as JSON }
				}
			} else {
				withFormat {
					json  {
						response.status = 403
						render "Not Exist"
					}
				}
			}
		}
	}

	@Transactional
	def sendProfile() {
		def profile = Profile.findByToken(params.token)

		if(!profile) {
			def newprofile = new Profile(params)
			newprofile.save flush:true, failOnError:true

			withFormat { json { respond newprofile } }
		} else {
			withFormat { json { respond profile } }
		}
	}
}
