package com.eximiomedia



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured
import java.io.File
import java.util.concurrent.ExecutionException
import java.text.SimpleDateFormat

@Secured(['ROLE_ADMIN'])
@Transactional(readOnly = true)
class MitosfaktaController {

    static allowedMethods = [save: "POST", update: "POST", delete: "DELETE"]
	
	def androidGcmService
	
	def amazonS3Service

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Mitosfakta.list(params), model:[mitosfaktaInstanceCount: Mitosfakta.count()]
    }

    def show(Mitosfakta mitosfaktaInstance) {
        respond mitosfaktaInstance
    }

    def create() {
        respond new Mitosfakta(params)
    }

    @Transactional
    def save(Mitosfakta mitosfaktaInstance) {
		def file = request.getFile('mfImageFile')
		if(!file.empty) {
			def ftype
			if(file.contentType.equals("image/png"))
			ftype="png"
			if(file.contentType.equals("image/jpg"))
			ftype="jpg"
			if(file.contentType.equals("image/gif"))
			ftype="gif"
			if(file.contentType.equals("image/jpeg"))
			ftype="jpg"
			def webRootDir = servletContext.getRealPath("/")
			def userDir = new File(webRootDir, "/temp")
			def rname = new SimpleDateFormat("ddMMyyHHmmss").format(new Date())
			def fname = "mitfak-"+rname+"."+ftype
			userDir.mkdirs()
			file.transferTo(new File(userDir, fname))
			try {
				def dir = new File(webRootDir, "/temp")
				File tempPicture = new File(dir,fname)
				amazonS3Service.putFile ("articles.appdokter.com",tempPicture)
				tempPicture.delete()
			} catch(InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
			mitosfaktaInstance.mfImage = fname
		}
		
        if (mitosfaktaInstance == null) {
            notFound()
            return
        }

        if (mitosfaktaInstance.hasErrors()) {
            respond mitosfaktaInstance.errors, view:'create'
            return
        }

        mitosfaktaInstance.save flush:true
		
//		def appUser = AppUser.list()
//		
//		def mitfaktitle
//		if(mitosfaktaInstance.mfTitle.size()>40) {
//			mitfaktitle = mitosfaktaInstance.mfTitle[0..40]
//		} else {
//			mitfaktitle = mitosfaktaInstance.mfTitle
//		}
//		Map msg = new HashMap()
//		msg.put("message", mitfaktitle)
//		msg.put("title","New Myth & Fact")
//		msg.put("msgcnt","4")
//		
//		List devices = new ArrayList()
//		
//		appUser.each { u ->
//			devices.add(u.regid)
//		}
//		
//		androidGcmService.sendMessage(msg, devices);

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'mitosfaktaInstance.label', default: 'Mitosfakta'), mitosfaktaInstance.id])
                redirect mitosfaktaInstance
            }
            '*' { respond mitosfaktaInstance, [status: CREATED] }
        }
    }

    def edit(Mitosfakta mitosfaktaInstance) {
        respond mitosfaktaInstance
    }

    @Transactional
    def update(Mitosfakta mitosfaktaInstance) {
		def file = request.getFile('mfImageFile')
		if(!file.empty) {
			if(mitosfaktaInstance.mfImage) {
				amazonS3Service.delFile ("articles.appdokter.com",mitosfaktaInstance.mfImage)
			}
			def ftype
			if(file.contentType.equals("image/png"))
			ftype="png"
			if(file.contentType.equals("image/jpg"))
			ftype="jpg"
			if(file.contentType.equals("image/gif"))
			ftype="gif"
			if(file.contentType.equals("image/jpeg"))
			ftype="jpg"
			def webRootDir = servletContext.getRealPath("/")
			def userDir = new File(webRootDir, "/temp")
			def rname = new SimpleDateFormat("ddMMyyHHmmss").format(new Date())
			def fname = "mitfak-"+rname+"."+ftype
			userDir.mkdirs()
			file.transferTo(new File(userDir, fname))
			try {
				def dir = new File(webRootDir, "/temp")
				File tempPicture = new File(dir,fname)
				amazonS3Service.putFile ("articles.appdokter.com",tempPicture)
				tempPicture.delete()
			} catch(InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
			mitosfaktaInstance.mfImage = fname
		}
		
        if (mitosfaktaInstance == null) {
            notFound()
            return
        }

        if (mitosfaktaInstance.hasErrors()) {
            respond mitosfaktaInstance.errors, view:'edit'
            return
        }

        mitosfaktaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Mitosfakta.label', default: 'Mitosfakta'), mitosfaktaInstance.id])
                redirect mitosfaktaInstance
            }
            '*'{ respond mitosfaktaInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Mitosfakta mitosfaktaInstance) {

        if (mitosfaktaInstance == null) {
            notFound()
            return
        }

        mitosfaktaInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Mitosfakta.label', default: 'Mitosfakta'), mitosfaktaInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'mitosfaktaInstance.label', default: 'Mitosfakta'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
