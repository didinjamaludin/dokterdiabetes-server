package com.eximiomedia



class Notif1Job {
    
	def bukaSahurService

	static triggers = {
		cron name: 'cronTrigger', cronExpression: "0 30 3 ? * *"
	}

	def execute() {
		bukaSahurService.sahurNotif()
	}
}
