package com.eximiomedia



class Notif3Job {
    
	def bukaSahurService

	static triggers = {
		cron name: 'cronTrigger', cronExpression: "0 0 18 ? * *"
	}

	def execute() {
		bukaSahurService.bukaNotif()
	}
}
