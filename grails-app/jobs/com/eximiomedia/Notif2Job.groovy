package com.eximiomedia



class Notif2Job {

	def mitosFaktaService
	def articleService

	static triggers = {
		cron name: 'cronTrigger', cronExpression: "0 30 8 ? * *"
	}

	def execute() {
		mitosFaktaService.mitosfaktaNotification()
		articleService.articleNotification()
	}
}
